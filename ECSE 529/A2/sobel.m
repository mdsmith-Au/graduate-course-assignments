function [ ] = sobel()
%SOBEL Sobel edge detector

plant = imread('plant.pgm');
tower = imread('tower.pgm');
city = imread('city.pgm');

names = {'plant','tower','city'};

list = {plant,tower,city};

for ind=1:3
    I = list{ind};
    
    % 3x3 case
    % First grow image by 1 pixel row wise
%     I2 = [ I(1,:); I; I(end,:)];
%     % Then column wise
%     I2 = single([ I2(:,1), I2, I2(:,end)]);

    % 5x5 case
    % First grow image by 2 pixels row wise
    I2 = [ I(1,:); I; I(end,:)];
    I2 = [ I2(1,:); I2; I2(end,:)];
    % Then column wise
    I2 = single([ I2(:,1), I2, I2(:,end)]);
    I2 = [ I2(:,1), I2, I2(:,end)];
    
    % Define sobel operator
    % 3x3x
    %sobelx = [ 1 0 -1; 2 0 -2; 1 0 -1];
    % 3x3y
    %sobely = [ -1 -2 -1; 0 0 0; 1 2 1];
    % 5x5x
    sobelx = [ 1 2 0 -2 -1; 4 8 0 -8 -4; 6 12 0 -12 -6; 4 8 0 -8 -4; 1 2 0 -2 -1];
    % 5x5y
    sobely = -[ 1 2 0 -2 -1; 4 8 0 -8 -4; 6 12 0 -12 -6; 4 8 0 -8 -4; 1 2 0 -2 -1]';

    imgSize = size(I2);

    resultImgX = zeros(imgSize);
    resultImgY = zeros(imgSize);
    % 3x3 case
    % First loop by column
%     for i = 2:(imgSize(2) - 1)
%         % Then row
%         for j = 2:(imgSize(1) - 1)
%             % Run filter: calculate dot product of a small
%             % square of the image with the filter, sum it
%             resultImgX(j,i) = sum(dot(I2(j-1:j+1,i-1:i+1), sobelx));
%             resultImgY(j,i) = sum(dot(I2(j-1:j+1,i-1:i+1), sobely));
%         end
%     end

    % 5x5 case
    for i = 3:(imgSize(2) - 2)
        % Then row
        for j = 3:(imgSize(1) - 2)
            % Run filter: calculate dot product of a small
            % square of the image with the filter, sum it
            resultImgX(j,i) = sum(dot(I2(j-2:j+2,i-2:i+2), sobelx));
            resultImgY(j,i) = sum(dot(I2(j-2:j+2,i-2:i+2), sobely));
        end
    end



    % Strip extra edge pixels
    % 3x3
    %resultImgX = resultImgX(2:end-1,2:end-1);
    %resultImgY = resultImgY(2:end-1,2:end-1);
    % 5x5
    resultImgX = resultImgX(3:end-2,3:end-2);
    resultImgY = resultImgY(3:end-2,3:end-2);
    
    resultImg = sqrt(resultImgX.^2 + resultImgY.^2);
    
    R = [0 255];
    dR = diff( R );

    resultImg =  resultImg - min( resultImg(:)); % set range of A between [0, inf)
    resultImg =  resultImg ./ max( resultImg(:)) ; % set range of A between [0, 1]
    resultImg =  resultImg .* dR ; % set range of A between [0, dRange]
    resultImg =  resultImg + R(1); % shift range of A to R
    
    
    imwrite(uint8(resultImg), strcat(names{ind},'_sobel55.png'));
end


end


function [ edges ] = canny( I, name )
% CANNY Canny edge detector
% First grow image by 1 pixel row wise
I2 = [ I(1,:); I; I(end,:)];
% Then column wise
I2 = single([ I2(:,1), I2, I2(:,end)]);

% Define gaussian of 3x3 with sigma = 2
gauss = fspecial('gauss',[3 3], 2);
 
imgSize = size(I2);

resultImg = zeros(imgSize);
% First loop by column
for i = 2:(imgSize(2) - 1)
    % Then row
    for j = 2:(imgSize(1) - 1)
        % Run filter: calculate dot product of a small
        % square of the image with the filter, sum it
        resultImg(j,i) = sum(dot(I2(j-1:j+1,i-1:i+1), gauss));
    end
end

% Strip extra edge pixels
resultImg = resultImg(2:end-1,2:end-1,:);

% Define gradient kernels
Gx = [-1 1; -1 1];
Gy = [ 1 1; -1 -1];

% Grow image by 1 pixel row wise
resultGradientX = [ resultImg(1,:); resultImg; resultImg(end,:)];
resultGradientY = resultGradientX;
% Then column wise
resultGradientX = single([ resultGradientX(:,1), resultGradientX, resultGradientX(:,end)]);
resultGradientY = single([ resultGradientY(:,1), resultGradientY, resultGradientY(:,end)]);

imgSize = size(resultGradientX);

% First loop by column
for i = 2:imgSize(2)
    % Then row
    for j = 2:imgSize(1)
        % Run filter: calculate dot product of a small
        % square of the image with the filter, sum it
        resultGradientX(j,i) = sum(dot(I2(j-1:j,i-1:i), Gx))/2;
        resultGradientY(j,i) = sum(dot(I2(j-1:j,i-1:i), Gy))/2;
    end
end

% Strip extra pixels
resultGradientX = resultGradientX(2:end-1,2:end-1,:);
resultGradientY = resultGradientY(2:end-1,2:end-1,:);


%figure('Name','X');
%imshow(uint8(resultGradientX));
%figure('Name','Y');
%imshow(uint8(resultGradientY));

% Compute magnitude/orientation images
M = sqrt(resultGradientX.^2 + resultGradientY.^2);
Theta = atan2(resultGradientY,resultGradientX);
Theta0ToPi = Theta + pi;

display = uint8(round(M./(max(M(:))/255)));
imwrite(display,strcat(name,'_magnitude.png'));

display = uint8(round(Theta0ToPi./(max(Theta0ToPi(:))/255)));
imwrite(display,strcat(name,'_phase.png'));

% Nonmax suppression - calculate Zeta (Theta -> 4 quadrants)
% First, convert angle into 0-3
Zeta = zeros(size(Theta),'single');
Zeta(Theta0ToPi >= 0 & Theta0ToPi <= pi/8) = 0;
Zeta(Theta0ToPi > pi/8 & Theta0ToPi <= 3*pi/8) = 1;
Zeta(Theta0ToPi > 3*pi/8 & Theta0ToPi <= 5*pi/8) = 2;
Zeta(Theta0ToPi > 5*pi/8 & Theta0ToPi <= 7*pi/8) = 3;
Zeta(Theta0ToPi > 7*pi/8 & Theta0ToPi <= 9*pi/8) = 0;
Zeta(Theta0ToPi > 9*pi/8 & Theta0ToPi <= 11*pi/8) = 1;
Zeta(Theta0ToPi > 11*pi/8 & Theta0ToPi <= 13*pi/8) = 2;
Zeta(Theta0ToPi > 13*pi/8 & Theta0ToPi <= 15*pi/8) = 3;
Zeta(Theta0ToPi > 15*pi/8 & Theta0ToPi <= 16*pi/8) = 0;

% Grow M, Zeta by 1 pixel row wise
M = [ M(1,:); M; M(end,:)];
Zeta = [ Zeta(1,:); Zeta; Zeta(end,:)];
% Then column wise
M = single([ M(:,1), M, M(:,end)]);
Zeta = single([ Zeta(:,1), Zeta, Zeta(:,end)]);


imgSize = size(M);

N = zeros(imgSize,'single');

% Non max suppression
for i = 2:imgSize(2)-1
    for j = 2:imgSize(1)-1
        if Zeta(j,i) == 0
            neighbours = [M(j,i-1) M(j,i+1)];
        elseif Zeta(j,i) == 1
            neighbours = [M(j+1,i+1) M(j-1,i-1)];
        elseif Zeta(j,i) == 2
            neighbours = [M(j+1,i) M(j-1,i)];
        elseif Zeta(j,i) == 3
            neighbours = [M(j-1,i+1) M(j+1,i-1)];
        end
        
        if ((M(j,i) > neighbours(1)) && (M(j,i) > neighbours(2)))
            N(j,i) = M(j,i);
        end
    end
end


N_minusedges = N(2:end-1,2:end-1,:);
display = uint8(round(N_minusedges./(max(N_minusedges(:))/255)));
imwrite(display,strcat(name,'_nonmax.png'));

% Threshold w/ T1 and T2
% Note that the extra edge pixels are left in N
N = uint8(round(N));
T2 = 60;
T1 = T2/2;

I_T1 = im2bw(N,T1/255);
I_T2 = im2bw(N,T2/255);

display = uint8(round(I_T1./(max(I_T1(:))/255)));
imwrite(display,strcat(name,'_T1.png'));

display = uint8(round(I_T2./(max(I_T2(:))/255)));
imwrite(display,strcat(name,'_T2.png'));

% Initialize a cell array to all 1 to represent unvisited pixels
% Iterate over thresholded image T2, mark pixel as visited
% Only take action if pixel = 1
% If 1, get direction
% Use direction to find neighbours
% See if neighbours do NOT exist in T2
% If NO values in T2, see if T1 has a value in either neighbour pixel
% If it does, import those 1 or 2 pixels into T2
% Mark those pixels as UNvisited
% Continue to next T2 pixel
% Loop entire thing over until sum of unvisited = 0

imgSize = size(N);

unvisited = ones(imgSize,'uint8');
while sum(unvisited(:)) ~= 0
    for i = 1:imgSize(2)
        for j = 1:imgSize(1)
            % Pixel is unvisited; visit
            if unvisited(j,i) == 1
                unvisited(j,i) = 0;
                % If pixels is an edge in T2...
                if I_T2(j,i) == 1
                    % Get neighbours - note that we go along gradient not
                    % perpendicular to it as before
                    if Zeta(j,i) == 0
                        neighbours = {[j+1,i],[j-1,i]};
                        neighboursT2 = [I_T2(j+1,i) I_T2(j-1,i)];
                        neighboursT1 = [I_T1(j+1,i) I_T1(j-1,i)];
                    elseif Zeta(j,i) == 1
                        neighbours = {[j-1,i+1],[j+1,i-1]};
                        neighboursT2 = [I_T2(j-1,i+1) I_T2(j+1,i-1)];
                        neighboursT1 = [I_T1(j-1,i+1) I_T1(j+1,i-1)];
                    elseif Zeta(j,i) == 2
                        neighbours = {[j,i-1],[j,i+1]};
                        neighboursT2 = [I_T2(j,i-1) I_T2(j,i+1)];
                        neighboursT1 = [I_T1(j,i-1) I_T1(j,i+1)];
                    elseif Zeta(j,i) == 3
                        neighbours = {[j+1,i+1],[j-1,i-1]};
                        neighboursT2 = [I_T2(j+1,i+1) I_T2(j-1,i-1)];
                        neighboursT1 = [I_T1(j+1,i+1) I_T1(j-1,i-1)];
                    end
                    
                    % If neighbours aren't in T2....
                    if neighboursT2(1) == 0
                        if neighboursT1(1) ~= 0
                            % Add from T1
                            I_T2(neighbours{1}(1),neighbours{1}(2)) = 1;
                            % Mark as unvisited
                            unvisited(neighbours{1}(1),neighbours{1}(2)) = 1;
                        end
                    end
                    if neighboursT2(2) == 0
                        if neighboursT1(2) ~= 0
                            % Add from T1
                            I_T2(neighbours{2}(1),neighbours{2}(2)) = 1;
                            % Mark as unvisited
                            unvisited(neighbours{2}(1),neighbours{2}(2)) = 1;
                        end
                    end
                    
                end
            end
        end
    end
end

% Remove excess pixels
I_T2 = I_T2(2:end-1,2:end-1,:);

edges = I_T2;

imwrite(edges,strcat(name,'_final.png'));
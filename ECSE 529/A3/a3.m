function [  ] = a3(  )

%NOTE:  load VLFEAT library before running
%NOTE2: add "ojwoodford..." to path before running (display utility
%library)


% Load images
set = imageSet('training');

SIFTfeatures = [];

LBPfeatures = [];

HoGfeatures = [];


% Calculate all features
for i = 1 : set.Count
    img = imread(set.ImageLocation{i});
    [SIFTF, ~, ~] = BagOfFeaturesExtractorSIFT(img);
    [LBPF, ~, ~] = BagOfFeaturesExtractorLBP(img);
    [HoGF, ~, ~] = BagOfFeaturesExtractorHoG(img);
    SIFTfeatures = [SIFTfeatures, SIFTF];
    LBPfeatures = [LBPfeatures, LBPF];
    HoGfeatures = [HoGfeatures, HoGF];
end

% Run Kmeans to cluster all features
numClusters = 512 ;
[LBPcenters, LBPassignments] = vl_kmeans(LBPfeatures, numClusters);
[HoGcenters, HoGassignments] = vl_kmeans(HoGfeatures, numClusters);
[SIFTcenters, SIFTassignments] = vl_kmeans(SIFTfeatures, numClusters);

% Merge the three assignment vectors
% Note that SIFT = 1-512,Hog:513-1024,LBP:1025-1536
allassign = [SIFTassignments, HoGassignments + numClusters, LBPassignments + (numClusters*2) ];

% Count number of occurences
occur = histcounts(allassign,numClusters*3);

% Sort..
[~, sortIdx] = sort(occur,'descend');

% Chop off anything after 256
sortIdx = sortIdx(1:256);

% Sort it in order for later when we apply color
sortIdx = sort(sortIdx,'descend');


% So we will encode a new 10 image dataset using the three feature types
testset = imageSet('./report');

for i = 1 : testset.Count
    img = imread(testset.ImageLocation{i});
    [SIFTF, ~, SIFTL] = BagOfFeaturesExtractorSIFT(img);
    [LBPF, ~, LBPL] = BagOfFeaturesExtractorLBP(img);
    [HoGF, ~, HoGL] = BagOfFeaturesExtractorHoG(img);

    % Need to compute dist to *all* centers from original
    [~, kS] = min(vl_alldist(SIFTF, SIFTcenters),[],2) ;
    [~, kH] = min(vl_alldist(HoGF, HoGcenters),[],2) ;
    kH = kH + numClusters;
    [~, kL] = min(vl_alldist(LBPF, LBPcenters),[],2) ;
    kL = kL + numClusters*2;
    
    % So now we have mappings for all 1500+ feature centers
    % Of course we will be using only 256 of these
    % We iterate over all the 256 "good" features
    displayImg = zeros(size(img),'single');
    numSIFTfeatures = 0;
    numHoGfeatures = 0;
    numLBPfeatures = 0;
    for j = 1:size(sortIdx,2)
        value = sortIdx(j);
        % SIFT feature
        if value > 1 && value < 513
            % Find all matches
            matches = find(kS == value);
            % Get x,y position of matches
            xy = SIFTL(:,matches);
            % Set image at those positions to the color of j
            idx = sub2ind(size(displayImg), xy(2,:), xy(1,:));
            displayImg(idx) = j;
            numSIFTfeatures = numSIFTfeatures + 1;
            % HoG feature
        elseif value > 512 && value < 1025
            % Find all matches
            matches = find(kH == value);
            % Get x,y position of matches
            xy = round(HoGL(:,matches));
            % Set image at those positions to the color of j
            idx = sub2ind(size(displayImg), xy(2,:), xy(1,:));
            displayImg(idx) = j;
            numHoGfeatures = numHoGfeatures + 1;
            % LBP feature
        else
            % Find all matches
            matches = find(kL == value);
            % Get x,y position of matches
            xy = round(LBPL(:,matches));
            % Set image at those positions to the color of j
            idx = sub2ind(size(displayImg), xy(2,:), xy(1,:));
            displayImg(idx) = j;
            numLBPfeatures = numLBPfeatures + 1;
        end
    end
    fprintf('Image %i has %i SIFT features, %i HoG features and %i LBP features.\n',i,numSIFTfeatures,numHoGfeatures,numLBPfeatures);
    imwrite(sc(displayImg,'jet'),strcat('output_',num2str(i),'.png'));
    
end



end
% SIFT extractor
function [features,featureMetrics,featureLocations] = BagOfFeaturesExtractorSIFT(img)

disp('Running SIFT...');

% Define bin size, magnification according to vl feat documentation
binSize = 8 ;
magnif = 3 ;
Ismooth = vl_imsmooth(im2single(img), sqrt((binSize/magnif)^2 - .25)) ;
[featureLocations,descriptors] = vl_dsift(Ismooth, 'size', binSize) ;

features = single(descriptors);
% Define variance as "strength" of descriptor
featureMetrics = var(single(descriptors));

% Select 4800 random features
perm = randperm(size(featureLocations,2)) ;
sel = perm(1:60*80) ;

features = features(:,sel);
featureMetrics = featureMetrics(:,sel);
featureLocations = featureLocations(:,sel);

% Old - select based on strength
% [featureMetrics,strongIdx] = sort(featureMetrics,'descend');
% featureMetrics = featureMetrics(1:60*80);
% strongIdx = strongIdx(1:60*80);
% features = features(:,strongIdx);
% featureLocations = featureLocations(:,strongIdx);
end

function [features,featureMetrics,featureLocations] = BagOfFeaturesExtractorHoG(img)
disp('Running HoG...');
binSize = 8 ;
hog = vl_hog(im2single(img), binSize, 'verbose');

% Features are in an array based on the img size / bin size
features = reshape(hog,[ size(hog,1)*size(hog,2), size(hog,3)])';
% Define variance as "strength" of descriptor although it is not used
featureMetrics = var(features);

% Create a matrix the size of the (original) feature matrix
featureLocations = zeros([size(hog,1) size(hog,2)],'single');
% And fill it with the x/y position of each 8x8 cell
for row = 1:size(hog,1)
    for col = 1:size(hog,2)
        featureLocations(row,col,1) = col*binSize - 3.5;
        featureLocations(row,col,2) = row*binSize - 3.5;
    end
end
% Reshape it the same way we reshaped the features to make a feature
% location map
featureLocations = reshape(featureLocations, [ size(featureLocations,1)*size(featureLocations,2), size(featureLocations,3)])';
end

function [features,featureMetrics,featureLocations] = BagOfFeaturesExtractorLBP(img)
disp('Running LBP...');
binSize = 8 ;
% Note that this LBP produces features using the binSize*binSize grid
% of length 58 since it uses the "uniform pattern" method
% (M. Heikkil�, M. Pietik�inen, "A texture-based method for modeling
% the background and detecting moving objects", IEEE Transactions on Pattern Analysis and Machine Intelligence)
lbp = vl_lbp(im2single(img), binSize);

% Features are in an array based on the img size / bin size
features = reshape(lbp,[ size(lbp,1)*size(lbp,2), size(lbp,3)])';
% Define variance as "strength" of descriptor although it is not used
featureMetrics = var(features);

% Create a matrix the size of the (original) feature matrix
featureLocations = zeros([size(lbp,1) size(lbp,2)],'single');
% And fill it with the x/y position of each 8x8 cell
for row = 1:size(lbp,1)
    for col = 1:size(lbp,2)
        featureLocations(row,col,1) = col*binSize - 3.5;
        featureLocations(row,col,2) = row*binSize - 3.5;
    end
end
% Reshape it the same way we reshaped the features to make a feature
% location map
featureLocations = reshape(featureLocations, [ size(featureLocations,1)*size(featureLocations,2), size(featureLocations,3)])';
end


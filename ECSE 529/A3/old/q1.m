function [  ] = q1(  )
%Acquire training data
% Note that this uses the vlfeat library to implement the feature detectors
% It must be set up prior to use; see
% http://www.vlfeat.org/install-matlab.html for details
%run('vlfeat-0.9.20/toolbox/vl_setup');

% Retrive images
set = imageSet('training');

% Run bag of features to create 3 codebook, 1 for each feature type
extractorSIFT = @BagOfFeaturesExtractorSIFT;
extractorHoG = @BagOfFeaturesExtractorHoG;
extractorLBP = @BagOfFeaturesExtractorLBP;
% LBP and HoG are fairly fast
bagLBP = bagOfFeatures(set,'CustomExtractor',extractorLBP,'VocabularySize',256,'StrongestFeatures',1);
bagHoG = bagOfFeatures(set,'CustomExtractor',extractorHoG,'VocabularySize',256,'StrongestFeatures',1);
% SIFT is slow and **very** memory intensive.  Make sure you run this on
% something with enough RAM like soma.cim.mcgill.ca.  My (old) laptop
% couldn't handle it - you may want to choose only 25-50% of the features
% in that case
bagSIFT = bagOfFeatures(set,'CustomExtractor',extractorSIFT,'VocabularySize',256,'StrongestFeatures',1);

% Save to disk so we can test out the results on new images later
save data.mat

end

% SIFT extractor
function [features,featureMetrics,featureLocations] = BagOfFeaturesExtractorSIFT(img)
    
    disp('Running SIFT...');
    
    % Define bin size, magnification according to vl feat documentation
    % Note that we first smooth the image
    binSize = 8 ;
    magnif = 3 ;
    Ismooth = vl_imsmooth(im2single(img), sqrt((binSize/magnif)^2 - .25)) ;
    [featureLocations,descriptors] = vl_dsift(Ismooth, 'size', binSize) ;
    
    % We need to flip the descriptors for MATLAB to handle
    features = single(descriptors)';
    % Define variance as "strength" of descriptor although it is not used
    % since we keep all features
    featureMetrics = var(single(descriptors))';
    % We also need to flip the feature locations
    featureLocations = featureLocations';

end

function [features,featureMetrics,featureLocations] = BagOfFeaturesExtractorHoG(img)
    disp('Running HoG...');
    binSize = 8 ;
    hog = vl_hog(im2single(img), binSize, 'verbose');

    % Features are in an array based on the img size / bin size
    % We need to reshape it into a matrix MATLAB can use
    features = reshape(hog,[size(hog,1)*size(hog,2) size(hog,3)]);
    % Define variance as "strength" of descriptor although it is not used
    % since we keep all features
    featureMetrics = var(features')';
    
    % Create a matrix the size of the (original) feature matrix
    featureLocations = zeros([size(hog,1) size(hog,2)],'single');
    % And fill it with the x/y position of each 8x8 cell
    for row = 1:size(hog,1)
        for col = 1:size(hog,2)
            featureLocations(row,col,1) = col*binSize - 3.5;
            featureLocations(row,col,2) = row*binSize - 3.5;
        end
    end
    % Reshape it the same way we reshaped the features to make a feature
    % location map
    featureLocations = reshape(featureLocations, [size(featureLocations,1)*size(featureLocations,2),size(featureLocations,3)]);
end

function [features,featureMetrics,featureLocations] = BagOfFeaturesExtractorLBP(img)
    disp('Running LBP...');
    binSize = 8 ;
    % Note that this LBP produces features using the binSize*binSize grid
    % of length 58 since it uses the "uniform pattern" method
    % (M. Heikkilä, M. Pietikäinen, "A texture-based method for modeling
    % the background and detecting moving objects", IEEE Transactions on Pattern Analysis and Machine Intelligence)
    lbp = vl_lbp(im2single(img), binSize);

    % Features are in an array based on the img size / bin size
    % We need to reshape it into a matrix MATLAB can use
    features = reshape(lbp,[size(lbp,1)*size(lbp,2) size(lbp,3)]);
    % Define variance as "strength" of descriptor although it is not used
    % since we keep all features
    featureMetrics = var(features')';
    
    % Create a matrix the size of the (original) feature matrix
    featureLocations = zeros([size(lbp,1) size(lbp,2)],'single');
    % And fill it with the x/y position of each 8x8 cell
    for row = 1:size(lbp,1)
        for col = 1:size(lbp,2)
            featureLocations(row,col,1) = col*binSize - 3.5;
            featureLocations(row,col,2) = row*binSize - 3.5;
        end
    end
    % Reshape it the same way we reshaped the features to make a feature
    % location map
    featureLocations = reshape(featureLocations, [size(featureLocations,1)*size(featureLocations,2),size(featureLocations,3)]);
end

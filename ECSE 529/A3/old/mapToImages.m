function [  ] = mapToImages(  )
% Show features in images

load data

% Load images
set = imageSet('report');

% Encode images
[~,wordsHoG]  = encode(bagHoG, set);
[~,wordsSIFT]  = encode(bagSIFT, set);
[~,wordsLBP]  = encode(bagLBP, set);

% HoG - iterate over images
HoGimg = {};
for i = 1:size(wordsHoG,1)
   wordIdx = wordsHoG(i).WordIndex;
   locations = wordsHoG(i).Location;
   
   img = zeros([480,640],'single');
   for word = 1:size(wordIdx,1)
       x = int32(locations(word,1));
       y = int32(locations(word,2));
       % Since features are based on a 8x8 area, we color the entire area
       % Add +1 to account for 0 being defined as no word (in SIFT at
       % least)
       img(y-3.5:y+3.5,x-3.5:x+3.5) = wordIdx(word)+1;
   end
   HoGimg{i} = img;
end

% LBP - iterate over images
LBPimg = {};
for i = 1:size(wordsLBP,1)
   wordIdx = wordsLBP(i).WordIndex;
   locations = wordsLBP(i).Location;
   
   img = zeros([480,640],'single');
   for word = 1:size(wordIdx,1)
       x = int32(locations(word,1));
       y = int32(locations(word,2));
       % Since features are based on a 8x8 area, we color the entire area
       % Add +1 to account for 0 being defined as no word (in SIFT at
       % least)
       img(y-3.5:y+3.5,x-3.5:x+3.5) = wordIdx(word)+1;
   end
   LBPimg{i} = img;
end

% SIFT - iterate over images
SIFTimg = {};
for i = 1:size(wordsSIFT,1)
   wordIdx = wordsSIFT(i).WordIndex;
   locations = wordsSIFT(i).Location;
   
   img = zeros([480,640],'single');
   for word = 1:size(wordIdx,1)
       x = int32(locations(word,1));
       y = int32(locations(word,2));
       % Add +1 to account for 0 being defined as no word (in SIFT at
       % least)
       img(y,x) = wordIdx(word)+1;
   end
   SIFTimg{i} = img;
end

% Use image display functions from ojwood library to replace MATLAB
% imagesc, imshow, etc. b/c they're easier for this kind of colormap work
for i = 1:size(SIFTimg,2)
   img = sc (SIFTimg{i},'jet');
   imwrite(img,strcat('SIFT_',num2str(i),'.png'));
   img = sc (LBPimg{i},'jet');
   imwrite(img,strcat('LBP_',num2str(i),'.png'));
   img = sc (HoGimg{i},'jet');
   imwrite(img,strcat('HoG_',num2str(i),'.png'));
end

end


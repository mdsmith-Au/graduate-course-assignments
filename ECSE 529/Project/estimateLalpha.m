function [ Lalpha ] = estimateLalpha( image )
%ESTIMATELALPHA Summary of this function goes here
%   Detailed explanation goes here

% Find pixels with max value
grayImg = rgb2gray(image);

% Black out bottom 5/6 of image to avoid getting max value on non-sky (just
% in case)
grayImg( int32(size(grayImg,1)/6) : end, :) = 0;
%Also black out some edge pixels on the edges
grayImg( 1:7,:) = 0;
grayImg(:,1:7) = 0;
grayImg(:,end-7:end) = 0;
[~,I] = max(grayImg(:));

[row,col] = ind2sub(size(grayImg), I);

Lalpha = double(image(row,col,:));
end


function [ image2 ] = removeIllumColor( alpha, imageD )
%REMOVEILLUMCOLOR Summary of this function goes here
%   Detailed explanation goes here

image2(:,:,1) = double(imageD(:,:,1))./alpha(1);
image2(:,:,2) = double(imageD(:,:,2))./alpha(2);
image2(:,:,3) = double(imageD(:,:,3))./alpha(3);

end


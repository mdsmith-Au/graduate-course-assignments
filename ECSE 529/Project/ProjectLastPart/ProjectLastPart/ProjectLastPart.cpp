// ProjectLastPart.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "mrf.h"
#include "ICM.h"
#include "GCoptimization.h"
#include "MaxProdBP.h"
#include "TRW-S.h"
#include "BP-S.h"

#include <opencv2\core.hpp>
#include <opencv2\imgcodecs.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <new>

int numRows, numCols, numDepth;
vector<vector<vector<float>>> phi;
float maxCedge;
int sumLalpha;

string type2str(int type) {
	string r;

	uchar depth = type & CV_MAT_DEPTH_MASK;
	uchar chans = 1 + (type >> CV_CN_SHIFT);

	switch (depth) {
	case CV_8U:  r = "8U"; break;
	case CV_8S:  r = "8S"; break;
	case CV_16U: r = "16U"; break;
	case CV_16S: r = "16S"; break;
	case CV_32S: r = "32S"; break;
	case CV_32F: r = "32F"; break;
	case CV_64F: r = "64F"; break;
	default:     r = "User"; break;
	}

	r += "C";
	r += (chans + '0');

	return r;
}


Mat estimateLalpha(Mat image) {
	cv::Mat greyMat;
	cv::cvtColor(image, greyMat, cv::COLOR_BGR2GRAY);

	Mat bottom = greyMat.rowRange(greyMat.rows / 6, greyMat.rows);
	bottom = bottom * 0;

	Mat edgeTop = greyMat.rowRange(1, 8);
	edgeTop = edgeTop * 0;

	Mat edgeLeft = greyMat.colRange(1, 8);
	edgeLeft = edgeLeft * 0;

	Mat edgeRight = greyMat.colRange(greyMat.cols - 8, greyMat.cols);
	edgeRight = edgeRight * 0;

	Point min_loc, max_loc;
	double min, max;
	minMaxLoc(greyMat, &min, &max, &min_loc, &max_loc);

	Mat pt = image.row(max_loc.y).col(max_loc.x);
	pt.convertTo(pt, CV_32FC3);

	Mat splitData[3];
	split(pt, splitData);

	Mat result = (Mat_<float>(3, 1) <<
		splitData[2].at<float>(0,0),
		splitData[1].at<float>(0, 0),
		splitData[0].at<float>(0, 0));

	return result;
}

Mat removeIllumColor(Mat alpha, Mat image) {
	Mat result = Mat::zeros(image.size(), CV_32FC3);

	image.convertTo(image, CV_32FC3);

	Mat channel[3];

	split(image, channel);

	channel[0] = channel[0] / alpha.at<float>(2, 0);
	channel[1] = channel[1] / alpha.at<float>(1, 0);
	channel[2] = channel[2] / alpha.at<float>(0, 0);

	merge(channel, 3, result);
	return result;
}


void dataCost(Mat Iprime, Mat Lalpha) {
	
	cout << "Calculating data cost..." << endl;
	int k = 20;
	int n = 5;
	int padAmount = 2;

	sumLalpha = (int) sum(Lalpha)[0];

	numRows = Iprime.rows;
	numCols = Iprime.cols;
	numDepth = sumLalpha - k;

	// constructs a larger image to fit both the image and the border
	Mat Ipad(Iprime.rows + padAmount * 2, Iprime.cols + padAmount * 2, Iprime.depth());

	copyMakeBorder(Iprime, Ipad, padAmount, padAmount, padAmount, padAmount, BORDER_REPLICATE);

	GaussianBlur(Ipad, Ipad, Size(0, 0), 1.5, 1.5, BORDER_DEFAULT);

	//Precompute
	Mat imageChannels[3];
	split(Ipad, imageChannels);

	vector<vector<Mat>> gradients;
	gradients.resize(numDepth);
	for (int i = 0; i < numDepth; ++i) {
		gradients[i].resize(3);
	}

	for (int Astar = 0; Astar < sumLalpha - k; Astar++) {

		float eBd = sumLalpha / (sumLalpha - Astar);

		Mat DgammaB, DgammaG, DgammaR;
		DgammaB = (imageChannels[0] - Astar)*eBd;
		DgammaG = (imageChannels[1] - Astar)*eBd;
		DgammaR = (imageChannels[2] - Astar)*eBd;

		/// Generate grad_x and grad_y
		Mat gradB_x, gradB_y, gradG_x, gradG_y, gradR_x, gradR_y, gradB, gradG, gradR;

		// Blue
		/// Gradient X
		Scharr(DgammaB, gradB_x, DgammaB.type(), 1, 0, 1, 0, BORDER_DEFAULT);
		gradB_x = abs(gradB_x);

		/// Gradient Y
		Scharr(DgammaB, gradB_y, DgammaB.type(), 0, 1, 1, 0, BORDER_DEFAULT);
		gradB_y = abs(gradB_y);

		/// Total Gradient (approximate)
		addWeighted(gradB_x, 0.5, gradB_y, 0.5, 0, gradB);

		// Green
		/// Gradient X
		Scharr(DgammaG, gradG_x, DgammaG.type(), 1, 0, 1, 0, BORDER_DEFAULT);
		gradG_x = abs(gradG_x);

		/// Gradient Y
		Scharr(DgammaG, gradG_y, DgammaG.type(), 0, 1, 1, 0, BORDER_DEFAULT);
		gradG_y = abs(gradG_y);

		/// Total Gradient (approximate)
		addWeighted(gradG_x, 0.5, gradG_y, 0.5, 0, gradG);

		//Red
		/// Gradient X
		Scharr(DgammaR, gradR_x, DgammaR.type(), 1, 0, 1, 0, BORDER_DEFAULT);
		gradR_x = abs(gradR_x);

		/// Gradient Y
		Scharr(DgammaR, gradR_y, DgammaR.type(), 0, 1, 1, 0, BORDER_DEFAULT);
		gradR_y = abs(gradR_y);

		/// Total Gradient (approximate)
		addWeighted(gradR_x, 0.5, gradR_y, 0.5, 0, gradR);

		gradients[Astar][0] = gradB;
		gradients[Astar][1] = gradG;
		gradients[Astar][2] = gradR;
	}

	phi.resize(numCols);
	for (int i = 0; i < numCols; ++i) {
		phi[i].resize(numRows);

		for (int j = 0; j < numRows; ++j)
			phi[i][j].resize(numDepth);
	}

	maxCedge = 0;


	for (int x = padAmount ; x < numCols - padAmount; x++) {

		for (int y = padAmount ; y < numRows - padAmount; y++) {

			for (int Astar = 0; Astar < sumLalpha - k; Astar++) {

				Mat gradB = gradients[Astar][0].rowRange(y - padAmount, y + padAmount + 1).colRange(x - padAmount, x + padAmount + 1);
				Mat gradG = gradients[Astar][1].rowRange(y - padAmount, y + padAmount + 1).colRange(x - padAmount, x + padAmount + 1);
				Mat gradR = gradients[Astar][2].rowRange(y - padAmount, y + padAmount + 1).colRange(x - padAmount, x + padAmount + 1);

				float Cedges = sum(gradR + gradG + gradB)[0];

				if (Cedges > maxCedge) {
					maxCedge = Cedges;
				}

				phi[x-padAmount][y-padAmount][Astar] = Cedges;
			}
		}
		cout << ((float)x / (float)numCols)*100 << "% complete" << endl;
	}
}

// Data term
 MRF::CostVal dCost(int pix, int i) {
	//int x = pix / numCols;
	//int y = pix % numCols;
	int row = pix / numCols;
	int col = pix % numCols;
	//cout << "pix: " << pix << endl << "i: " << i << endl;
	//cout << "x: " << x << endl << "y: " << y << endl;
	return  phi[col][row][i] / maxCedge;
}

//Smoothness term
 MRF::CostVal fnCost(int pix1, int pix2, int i, int j) {
	 return ((float)abs(i - j)/(float)(sumLalpha));
}

int main(int argc, char **argv)
{

	Mat image;
	image = imread("tokyo_input_small.jpg", CV_LOAD_IMAGE_COLOR);

	Mat Lalpha = estimateLalpha(image);

	/*Mat Lalpha = (Mat_<double>(3, 1) << 
		161,  
		194, 
		201);*/
	// alpha in R,G,B

	/*Mat alpha = (Mat_<double>(3, 1) <<
		0.289568345323741,
		0.348920863309353,
		 0.361510791366906);*/

	Mat alpha = Lalpha / (sum(Lalpha)[0]);

	Mat Iprime = removeIllumColor(alpha, image);

	Mat displayIprime = Iprime.clone();
	displayIprime = displayIprime / 3;
	displayIprime.convertTo(displayIprime, CV_8UC3);
	imwrite("Iprime.png", displayIprime);
	displayIprime.release();
	/*result = result / 3;
	result.convertTo(result, CV_8UC3);
	imshow("result", result);*/

	dataCost(Iprime, Lalpha);

	cout << "Beginning MRF optimization..." << endl;
	DataCost *data = new DataCost(dCost);
	SmoothnessCost *smooth = new SmoothnessCost(fnCost);
	EnergyFunction *eng = new EnergyFunction(data, smooth);

	float t;
	MRF* mrf = new Swap(numCols, numRows, numDepth, eng);
	mrf->initialize();
	mrf->clearAnswer();

	// Split Iprime into color channels
	Mat channels[3];
	split(Iprime, channels);

	channels[0] = 0.098*channels[0];
	channels[1] = 0.504*channels[1];
	channels[2] = 0.257*channels[2];

	Mat Y = channels[0] + channels[1] + channels[2];

	GaussianBlur(Y, Y, Size(0, 0), 4, 4, BORDER_DEFAULT);

	// Initialize to Y value
	for (int row = 0; row < numRows; row++) {
		for (int col = 0; col < numCols; col++) {
			mrf->setLabel(numCols * row + col, (int)Y.at<float>(row,col));
		}
	}

	Mat display;
	float Amin = *min_element(Y.begin<float>(), Y.end<float>());
	float Amax = *max_element(Y.begin<float>(), Y.end<float>());
	Mat A_scaled = (Y - Amin) / (Amax - Amin);
	A_scaled.convertTo(display, CV_8UC1, 255.0, 0);
	//applyColorMap(display, display, cv::COLORMAP_JET);
	//imshow("imagesc", display);
	//waitKey(0);

	mrf->optimize(5, t);  // run for 5 iterations, store time t it took 
	MRF::EnergyVal E_smooth = mrf->smoothnessEnergy();
	MRF::EnergyVal E_data = mrf->dataEnergy();

	//for (int pix = 0; pix < numCols*numRows; pix++) printf("Label of pixel %d is %d", pix, mrf->getLabel(pix));

	Mat A_map(numRows, numCols, CV_32FC1);

	for (int row = 0; row < numRows; row++) {
		for (int col = 0; col < numCols; col++) {
			//cout << "Label: " << mrf->getLabel(numCols * row + col) << endl;
			
			A_map.row(row).col(col) = (float)mrf->getLabel(numCols * row + col);
		}
	}

	Amin = *min_element(A_map.begin<float>(), A_map.end<float>());
	Amax = *max_element(A_map.begin<float>(), A_map.end<float>());
	A_scaled = (A_map - Amin) / (Amax - Amin);
	A_scaled.convertTo(display, CV_8UC1, 255.0, 0);
	//applyColorMap(display, display, cv::COLORMAP_JET);
	imwrite("map_A.png", display);
	//waitKey(0);
	//printf("Total Energy = %d (Smoothness energy %d, Data Energy %d)\n", E_smooth + E_data, E_smooth, E_data);
	//for (int pix = 0; pix < numCols*numRows; pix++) printf("Label of pixel %d is %d", pix, mrf->getLabel(pix));
	delete mrf;

	Mat eBd = -A_map + sumLalpha;
	eBd = sumLalpha / eBd;

	Mat channels2[3];
	split(Iprime, channels2);

	String ty = type2str(channels2[0].type());
	String ty2 = type2str(A_map.type());
	String ty3 = type2str(eBd.type());

	channels2[0] = (channels2[0] - A_map).mul(eBd);
	channels2[1] = (channels2[1] - A_map).mul(eBd);
	channels2[2] = (channels2[2] - A_map).mul(eBd);
	
	Mat finalImage;
	merge(channels2, 3, finalImage);

	/*double min, max;
	minMaxLoc(finalImage.reshape(1), &min, &max);

	finalImage = (finalImage / max)*255.0;*/

	finalImage = finalImage / 3;
	//cout << "Final: " << finalImage << endl;

	finalImage.convertTo(finalImage, CV_8UC3);
	imshow("final",finalImage);
	imwrite("finalImage.png", finalImage);
	waitKey(0);                
	return 0;

}

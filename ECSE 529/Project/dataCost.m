function [  ] = dataCost( Iprime, Lalpha )
%DATACOST Summary of this function goes here
%   Detailed explanation goes here

k = 20;

sumLalpha = sum(Lalpha);

n = 5;

padAmount = round(n/2 - 1);
% Pad image
IprimePad = padarray(Iprime,[padAmount padAmount], 'replicate');
phi = zeros([size(Iprime,1) size(Iprime,2) sumLalpha-k]);

for x = padAmount + 1: size(Iprime,2)-padAmount
    %phix = phi with x being a set value
    % It is indexed as y,Astar
    phiX = zeros([size(phi,1) 1 size(phi,3)]);
    for y = padAmount + 1: size(Iprime,1)-padAmount
        patch = IprimePad(y - padAmount : y + padAmount,x - padAmount : x + padAmount,:);
        % phiY = phi for a fixed x and y, index by Astar
        phiY = zeros([sumLalpha - k 1]);
        
        for Astar = 0 : sumLalpha - k - 1
            eBd = sumLalpha/(sumLalpha - Astar);
            
            % We compute for each pixel in the patch
            Dgamma = (patch - Astar*ones([5 5 3]))*eBd;
            
            [gradXR,gradYR] = gradient(Dgamma(:,:,1));
            [gradXG,gradYG] = gradient(Dgamma(:,:,2));
            [gradXB,gradYB] = gradient(Dgamma(:,:,3));
            
            gradXR = abs(gradXR);
            gradYR = abs(gradYR);
            
            gradXG = abs(gradXG);
            gradYG = abs(gradYG);
            
            gradXB = abs(gradXB);
            gradYB = abs(gradYB);
            
            Cedges = sum(gradXR(:) + gradYR(:) + gradXG(:) + gradYG(:) + gradXB(:) + gradYB(:));
            %% Normalize Cedges to 0->1 !!!!!!!!!!!!!!!!!
            phiY(Astar+1) = Cedges;
            %phi(y-padAmount,x-padAmount,Astar+1) =  Cedges;
        end
        phiX(y-padAmount,1,:) = phiY;
    end
    phi(:,x-padAmount,:) = phiX;
    fprintf('X at %f percent\n',(x/(size(Iprime,2)-padAmount))*100);
end
save('data.mat');
end








function [  ] = assignment1(  )
%Load images
lena_bw = rgb2gray(imread('lena BW.jpg'));
lena_color = imread('Lena_stasjon Color.jpg');

%% Part 1
% Compute threshold using Otsu's method
level = graythresh(lena_bw);

% Threshold...
binaryLena = im2bw(lena_bw, level);
figure('Name','Otsu method');
imshow(binaryLena, [1 0 0; 0 1 0]);

%% Part 2

% Get T1 from previous question
% It is normalized to 255; verified with debugger running through
% graythresh
T1 = level * 255;
% Get all pixels between T1 and 255
T1to255 = lena_bw(lena_bw >= T1 & lena_bw <= 255);
% Calculate threshold on these pixels
T2 = graythresh(T1to255);
% Threshold image
rosenBinary = im2bw(lena_bw, T2);
figure('Name','Rosen method');
imshow(rosenBinary, [1 0 0; 0 1 0]);

%% Part 3

figure('Name','Red channel histogram');
imhist(lena_color(:,:,1));

figure('Name','Green channel histogram');
imhist(lena_color(:,:,2));

figure('Name','Blue channel histogram');
imhist(lena_color(:,:,3));

%% Part 4
% Resize images to same height
lena_bw_resized = imresize(lena_bw,2);
lena_color_resized = imresize(lena_color,[size(lena_bw_resized,1) NaN]);
% Crop images to multiples of N=24x24
lena_bw_crop = imcrop(lena_bw_resized, [0 0 792 576 ]);
lena_color_crop = imcrop(lena_color_resized, [0 0  792 576 ]);

% Calculate number of rows/columns for blocks of size 3
numBlocksRows = 3 * ones(1, size(lena_bw_crop,1)/3);
numBlocksCols = 3 * ones(1, size(lena_bw_crop,2)/3);
%Subdivide...
bw_sub_3 = mat2cell(lena_bw_crop, numBlocksRows, numBlocksCols);
color_sub_3 = mat2cell(lena_color_crop, numBlocksRows, numBlocksCols,3);
% Calculate number of rows/columns for blocks of size 8
numBlocksRows = 8 * ones(1, size(lena_color_crop,1)/8);
numBlocksCols = 8 * ones(1, size(lena_color_crop,2)/8);
%Subdivide...
bw_sub_8 = mat2cell(lena_bw_crop, numBlocksRows, numBlocksCols);
color_sub_8 = mat2cell(lena_color_crop, numBlocksRows, numBlocksCols,3);

% Define 4 final thresholded images
bw3x3Thresh = false(size(lena_bw));
bw8x8Thresh = false(size(lena_bw));

color3x3Thresh = false(size(lena_bw));
color8x8Thresh = false(size(lena_bw));

% Find number of rows / columns in 3x3 division
numRows = size(bw_sub_3,1);
numCols = size(bw_sub_3,2);
% Process each square
for col = 1 : numCols
    for row = 1 : numRows
        % For the b/w image...
        
        % Calculate index in resultant thresholded image
        rowIdx = (row - 1)*3 + 1;
        colIdx = (col - 1)*3 + 1;
        
        % Get subdivision
        sqbw3 = bw_sub_3{row,col};
        % Compute Otsu's threshold
        T1_bw3 = graythresh(sqbw3) * 255;
        
        % Calculate using Rosin's method: get pixels from T1 -> 255
        pixelsbw3 = sqbw3(sqbw3 >= T1_bw3 & sqbw3 <= 255);
        % Calculate T2 using this
        T2_norm_bw3 = graythresh(pixelsbw3);
        
        % Save thresholded subdivision in final thresholded image
        bw3x3Thresh(rowIdx:rowIdx+2,colIdx:colIdx+2) = im2bw(sqbw3, T2_norm_bw3);
        
        
        %% For the color image..
        % Note that we combine the three channels into 1 here, giving what
        % is essentially extra data for the threshold computation
        sqcol3 = color_sub_3{row,col};
        
        T1_col3 = graythresh(sqcol3) * 255;
        
        pixelscol3 = sqcol3(sqcol3 >= T1_col3 & sqcol3 <= 255);
        T2_norm_col3 = graythresh(pixelscol3);
        
        color3x3Thresh(rowIdx:rowIdx+2,colIdx:colIdx+2) = im2bw(sqcol3, T2_norm_col3);
    end
end

% Find number of rows / columns in 8x8 division
numRows = size(bw_sub_8,1);
numCols = size(bw_sub_8,2);
% Process each square
for col = 1 : numCols
    for row = 1 : numRows
        
        % Get indices
        rowIdx = (row - 1)*8 + 1;
        colIdx = (col - 1)*8 + 1;
        
        % Calculate for b/w image
        sqbw8 = bw_sub_8{row,col};
        
        T1_bw8 = graythresh(sqbw8) * 255;
        pixelsbw8 = sqbw8(sqbw8 >= T1_bw8 & sqbw8 <= 255);
        
        T2_norm_bw8 = graythresh(pixelsbw8);
        
        bw8x8Thresh(rowIdx:rowIdx+7,colIdx:colIdx+7) = im2bw(sqbw8, T2_norm_bw8);
        
        % For color image
        % Note that we combine the three channels into 1 here, giving what
        % is essentially extra data for the threshold computation
        sqcol8 = color_sub_8{row,col};
        
        T1_col8 = graythresh(sqcol8) * 255;
        pixelscol8 = sqcol8(sqcol8 >= T1_col8 & sqcol8 <= 255);
        
        T2_norm_col8 = graythresh(pixelscol8);
        
        color8x8Thresh(rowIdx:rowIdx+7,colIdx:colIdx+7) = im2bw(sqcol8, T2_norm_col8);
    end
end



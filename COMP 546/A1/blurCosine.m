%  blurCosine.m
%
%  Repurposed from blurEccentric.m

%  Make an image which is a random texture defined by "1/f noise"
%  (which you need not try to understand at this point in the course).

N = 512;
I = makeTexture(N);
%load('I.mat');

maxSigma =  5;     %  sigma at a distance which is the max of the cos

sigmaStepSize   =  .1;   % we will quantize the sigma steps so that we can make use 
                         % of matlab's builtin conv function
                       
%  the sqrt(2) is used so that we blur all the way to the max                     
numSigmas   =   ceil(  sqrt(2) * maxSigma / sigmaStepSize );   

xvals = (1:N)' * ones(1,N);
yvals = ones(N,1) * (1:N);
xcenter = N/2;
ycenter = N/2;

% Define cosine
raisedCosine         = (make2Dcosine(N, 4, 0) + 1)/2;

%  Here is our new image whose blur will depend on eccentricity 

Iblur = zeros(N,N);
figure; 

%  Define another map which we will quantize into steps, 1, 2, 3, ...
%  like blinds
sigmaSteps = (maxSigma * raisedCosine) / sigmaStepSize;
    
for sigmaCt = 1:numSigmas

    %  Index the pixels whose sigma value,  maxSigma*raisedCosine,
    %  lies in the bin [sigmaStep *(sigmaCt - 1), sigmaStep *sigmaCt).
    %  The variable indx is a boolean array, used to select a subset of 
    %  pixels that satisfies some criterion.  
    
    indx = ( sigmaSteps >= sigmaCt - 1 ) & ( sigmaSteps < sigmaCt );

    %  Let sig be the quantized sigma value that is used for these pixels.

    sig = (sigmaCt-1)*sigmaStepSize;

    if (max(indx(:) > 0))     %  only bother if at least one pixel is indexed.

    %  Blur the whole image by sig.   
        
       gaussian2D = make2DGaussian(sig);
       IblurSig =  conv2( double(I), gaussian2D, 'same');  %  'same' makes output size
                                                           %  same as input size
       
    %  Assign the intensity values of the blur image to the indexed pixels.
    %  Non-indexed pixels keep their old values.
    
       Iblur = (1-indx) .* Iblur  +  indx .* IblurSig; 
 
    %  If you want to visualize how the image is constructed then 
    %  change the following line to "if (1)"

        if (0)
         imagesc( Iblur ); 
         colormap gray;  axis square; 
         pause(1);
         hold on;
       end
    end

end

IblurContrast = Iblur; 

%  We will use the 'image' function below which expects colormap indices.
%  The default colormap has 64 indices (1 to 64).
IblurContrast = uint8(ceil(IblurContrast*64));

Ifixate = ones(N,N)*32;

image(Ifixate); 
colormap gray; axis square;  hold on; pause(1);
image(IblurContrast); 
xlabel(['Iblur,    sigma at edge is ' num2str(maxSigma) ' pixels']);  
%pause(.5); close


 % clear

%  safer to clear all variables afterwards,  but I comment this out in case
%  you want to examine the values of the variables after program is done

% Contrast calculations
%areaBlurClean = double(ceil(I(:,256-32:256+32)*64));
%areaBlur = double(IblurContrast(:,256-32:256+32));

%contBlurClean = ( max(areaBlurClean(:)) - min(areaBlurClean(:)))/(max(areaBlurClean(:)) + min(areaBlurClean(:)));
%contBlur = ( max(area(:)) - min(area(:)))/(max(area(:)) + min(area(:)));

%variance = ( max(areaBlur(:)) - min(areaBlur(:)))/(max(areaBlurClean(:)) + min(areaBlurClean(:)))

% 
% for i = 3:N-2
%     for j = 3:N-2
%         area = double(IblurContrast(j-2:j+2,i-2:i+2));
%         cont(j,i) = ( max(area(:)) - min(area(:)))/(max(area(:)) + min(area(:)));
%     end
% end
% 
% cont = cont(3:N-2,3:N-2);
% variation = 0;
% for j = 1:N-4
%     scanLine = cont(j,:);
%     variation(j) = var(scanLine);
% end
% 
% meanVariation = mean(variation)
%  Comp 546 Assignment 1 
%  Solution Question 2

%  Make an image which is a random texture defined by "1/f noise"
%  (which you need not try to understand at this point in the course).

N             = 512;
Ibackground  = makeTexture(N);
%load('I.mat');
%Ibackground = I;

figure

%  Here we use a simple model of image blending in which a constant image
%  is blended with a texture/noise image.   

%  Let alpha be the "opacity" of a foreground layer which we will set to
%  be a cosine function and will have constant grey level intensity I_foreground.   
%  Note that  1-alpha is the "transparency" of that foreground layer, 
%  which is a multiple of the background (texture) layer.

%  A raised cosine function goes from 0 to 1.
raisedCosine         = (make2Dcosine(N, 4, 0) + 1)/2;

alphaMin = 0.0;       %  You need to change these values.
alphaMax = .5;       %       "

%  Define the opacity function for the foreground layer.
alpha = alphaMin + (alphaMax - alphaMin) * raisedCosine;

Iforeground = 1.0;    %  Do not change this.  (Making this smaller would give the 
                      %  foreground a dark color.)

%  Blend the (constant) foreground with the background texture.

display = (alpha * Iforeground +  (1-alpha) .* Ibackground ) *  255;
image( display );    

colormap(gray(255));
axis square

% Contrast calculations
%areaWhite = double(display(:,256-32:256+32));
%areaClean = double(Ibackground(:,256-32:256+32)*255);

%contClean = ( max(areaClean(:)) - min(areaClean(:)))/(max(areaClean(:)) + min(areaClean(:)));
%contWhite = ( max(areaWhite(:)) - min(areaWhite(:)))/(max(areaWhite(:)) + min(areaWhite(:)));

%cont = ( max(areaWhite(:)) - min(areaWhite(:)))/(max(areaClean(:)) + min(areaClean(:)))

% 
% for i = 3:N-2
%     for j = 3:N-2
%         area = double(display(j-2:j+2,i-2:i+2));
%         cont(j,i) = ( max(area(:)) - min(area(:)))/(max(area(:)) + min(area(:)));
%     end
% end
% 
% cont = cont(3:N-2,3:N-2);
% variation = 0;
% for j = 1:N-4
%     scanLine = cont(j,:);
%     variation(j) = var(scanLine);
% end
% 
% meanVariation = mean(variation)
%   color.m
%
%   author:  Michael Langer
%   
%   Assignment 1  COMP 546/598  Fall 2015

close all
clear

I = imread('Q1-Example.jpg');


sizeI = size(I);
Nrows = sizeI(1);
Ncols = sizeI(2);

%  save the redgreenB channels as double arrays.  This will be convenient for
%  vectorization later.

IRed   = double( I(:,:,1));
IGreen = double( I(:,:,2));
IBlue  = double( I(:,:,3));

figure
image(I)

%  Here we define the luminance, red-green, and blue-yellow axes

Vlum = [1, 1, 1];  
Vlum = Vlum / norm(Vlum); 

%  Note that something that is more red than green will be positive,
%  and more green than red will be negative

Vredgreen  = [ 1 -1 0 ] ;
Vredgreen = Vredgreen / norm(Vredgreen); 

%  More blue than yellow will be positive,
%  and more yellow than blue will be negative

Vblueyellow  = [ -1  -1   2 ] ;
Vblueyellow = Vblueyellow / norm(Vblueyellow); 
 
V = [Vlum; Vredgreen; Vblueyellow];

%  rotate redgreenB to luminance

lum =     Vlum(1) * IRed + ...         %  Nrows x Ncols 
          Vlum(2) * IGreen + ...
          Vlum(3) * IBlue;   

      
redgreen =      Vredgreen(1) * IRed + ...         %  Nrows x Ncols 
                Vredgreen(2) * IGreen + ...
                Vredgreen(3) * IBlue;   

blueyellow =  Vblueyellow(1) * IRed + ...         %  Nrows x Ncols 
              Vblueyellow(2) * IGreen + ...
              Vblueyellow(3) * IBlue;


figure          
im = remapImageUint8(lum);
image(im)
colormap(gray(256)) 
title(' luminance ')
%imwrite(im, 'lum.jpg');    %  prints image only (no title)
print('lum', '-djpeg ');   %  prints figure (with title)          
      
figure          
im = remapImageUint8(redgreen);
image(im)
colormap(gray(256)) 
title(' red-green ')
%imwrite(im, 'red-green.jpg');    %  prints image only (no title)
print('red-green', '-djpeg ')

figure          
im = remapImageUint8(blueyellow);
image(im)
colormap(gray(256)) 
title(' blue-yellow ')
%imwrite(im, 'blue-yellow.jpg');    %  prints image only (no title)
print('blue-yellow', '-djpeg ')


%---------   ADD YOUR CODE HERE  ---------------
%%%% Question 1a

scale = lum ./ 128;

IRed2 = IRed ./ scale;
IGreen2 = IGreen ./ scale;
IBlue2 = IBlue ./ scale;

chromaticity(:,:,1) = IRed2;
chromaticity(:,:,2) = IGreen2;
chromaticity(:,:,3) = IBlue2;
chromaticity = remapImageUint8(chromaticity);
imshow(chromaticity);
imwrite(chromaticity,'chromaticity-only.jpg');

%  Solution for Q1b

sobelX = single(fspecial('sobel'));
sobelY = single(-fspecial('sobel')');

dX_L = conv2(lum,sobelX,'same');
dY_L = conv2(lum,sobelY,'same');

dX_RG = conv2(redgreen,sobelX,'same');
dY_RG = conv2(redgreen,sobelY,'same');

dX_BL = conv2(blueyellow,sobelX,'same');
dY_BL = conv2(blueyellow,sobelY,'same');

m_L = sqrt(dX_L.^2 + dY_L.^2);
m_RG = sqrt(dX_RG.^2 + dY_RG.^2);
m_BL = sqrt(dX_BL.^2 + dY_BL.^2);

m_L = remapImageUint8(m_L > 2*std(m_L(:)));
m_RG = remapImageUint8(m_RG > 2*std(m_RG(:)));
m_BL = remapImageUint8(m_BL > 2*std(m_BL(:)));

figure; imshow(m_L); title 'Luminance Edge Map'; print('lum_edge', '-djpeg '); 
figure; imshow(m_RG); title 'Red-green Edge Map'; print('RG_edge', '-djpeg ');
figure; imshow(m_BL); title 'Blue-yellow Edge Map'; print('BL_edge', '-djpeg ');

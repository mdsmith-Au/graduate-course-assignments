%  Assignment 2  COMP 546/598  Fall 2015
%
%  Create a simple random dot stereogram with left and right images
%  Ileft and Iright.  Filter the stereo pair with a family of 
%  binocular complex Gabors.  

clear all
close all
N = 256;
disparity = 16;

%  Make a random dot stereogram with a central square protruding from a 
%  plane.  The plane has disparity = 0.  The central square has positive
%  disparity.

disparityInCm = disparity/1920 * 52;
distToSquare = 6.5 * 1.7/disparityInCm;
blurAngle = 0.5/distToSquare;
sigma = blurAngle*50;

randomPatt = rand(N,N);
randBlurred = imgaussfilt(randomPatt, sigma);

Ileft = randBlurred;
Iright = Ileft;
Iright(N/4:3*N/4, N/4:3*N/4) = randomPatt(N/4:3*N/4, N/4  + disparity: ...
        3*N/4 + disparity);
Iright(N/4:3*N/4, 3*N/4+1:3*N/4+disparity) = rand(N/2+1, disparity);

figure
I = zeros(N,N,3);
I(:,:,1) = Ileft;  % red
I(:,:,2) = Iright; %  cyan
I(:,:,3) = Iright;  % cyan

image(I);
title(['disparity is ' num2str(disparity)  ' pixels'])
axis square
axis off

%  Here we want the left and right Gabor cells to be shifted relative 
%  to each other.   If the shift is 0, then the cell is tuned to 0 
%  disparity as was the case looked at in the lecture.  If you want 
%  a different disparity then you need to shift by some other amount
%  which I am calling d_Gabor.
%
%  The binocular cell's response at (x,y) is the amplitude of the sum 
%  of responses of the left and right images Gabor cell.   To compute 
%  the binocular response, convolve the left image and right image 
%  complex GaborS.    To have a binocular cell that has a preferred 
%  disparity,  we need to shift the cell in one of the images.
%  Equivalently, we can shift the image (in the opposite direction) and
%  keep the cell position fixed.  The code below does the latter, which
%  is easier to deal with because then the corresponding left and right
%  cells are aligned.

M = 32;   % window width on which Gabor is defined
%M = 16;   % window width on which Gabor is defined
k = 2;    % frequency
%  wavelength of underlying sinusoid is M/k pixels per cycle.

[cosGabor sinGabor] = make2DGabor(M,0,k);
Gabor = cosGabor + 1i * sinGabor;
 
%  Subtract the mean intensity from the image because a cos Gabor 
%  in fact has a response to the mean intensity 
%  i.e.  (F cosGabor)(k) ~= 0  for k=0.     

Ileft  = Ileft - mean(Ileft(:));
Iright = Iright - mean(Iright(:));

figure
responses = zeros(N,N,9);

%  disparity of Gabors are tuned for  {-8,-7,...7,8}
numdisparities = 17;   %  from -8 to 8

for j = 1:numdisparities
    d_Gabor = j- (numdisparities+1)/2 ;   
    responses(:,:,j) = ...
        abs( circshift( conv2( Ileft, Gabor, 'same' ), [0 -d_Gabor])  ...
                         + conv2( Iright, Gabor , 'same')) ;
end

%  The images has now been filtered using a family of binocular complex
%  Gabor cells.  Each family has particular peak disparity to which
%  it is sensitive.    To plot the responses in a way that allows us
%  us to compare their magnitudes, we normalize by the largest response
%  of all of the "cells".  

maxResponse = max( responses(:) );

for j = 1:numdisparities
    d_Gabor = j - 1;
    responses(:,:,j) = responses(:,:,j)/maxResponse; 
    if mod(j, 2) == 1
        subplot(3,3,(j+1)/2);
        subimage(squeeze(responses(:,:,j)));
        title( ['Gabor tuned to d=' num2str(d_Gabor-8)  ] );
        axis off
    end
end

x = zeros(numdisparities);
y_sur = zeros(numdisparities);
y_cent = zeros(numdisparities);
count = 1;
for j = 1:numdisparities
    center = responses(N/4:3*N/4-1, N/4:3*N/4-1,j);
    centerMean = mean(center(:));

    response1 = responses(1:N/4,:,j);
    response2 = responses(3*N/4+1:end,:,j);
    response3 = responses(N/4+1:3*N/4,1:N/4,j);
    response4 = responses(N/4+1:3*N/4,3*N/4+1:end,j);
    outside = [response1(:); response2(:); response3(:); response4(:)];
    outsideMean = mean(outside(:));
    
    x(count) = j- (numdisparities+1)/2 ;   
    y_sur(count) = outsideMean;
    y_cent(count) = centerMean;
    count = count + 1;
    
end

figure;
plot(x,y_sur); title(strcat('Surround mean: M = ',num2str(M)));
figure;
plot(x,y_cent); title(strcat('Center mean: M = ',num2str(M)));
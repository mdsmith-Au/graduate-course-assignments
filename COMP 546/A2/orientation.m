%  orientation.m
%
%  author:  Michael Langer  COMP 546  2015
%  Assignment 2  Question 1
%
%  This code reads in an image (or synthesizes an image) and analyzes
%  the orientation structure at each pixel.   It uses an HSV coding:
%  hue is peak orientation,  saturation is the dominance within the
%  pixel of the response at the best vs worst orientations.
%  The value is the peak response relative to the largest peak response in
%  the image.

clear;  close all

%  Test code.   
%  Create an image which is an annulus (ring).
%  The orientation detector will annulus from red (at horizontal) 
%  around the hue circle (ROYGBV), back to red (at 180 degrees
%  away, i.e. horizontal again.

if (0)
    readImage  ; 
else
    N = 512;
    I = zeros(N,N);
    minRadius = 110;
    maxRadius = 115;    % thickness = 6
    
    Iannulus = sqrt(power((-N/2:N/2-1)' * ones(1,N),2) + ...
             power(ones(N,1) * (-N/2:N/2-1),2));
    Iannulus = double( (Iannulus >= minRadius) & (Iannulus <= maxRadius) );
    I = Iannulus;
    I(N/2 - maxRadius: N/2 + maxRadius, N/2-1:N/2+1) = 1;  % thickness = 3
    I(N/2-1:N/2+1, N/2 - maxRadius: N/2 + maxRadius) = 1;  
    
end

%  Show the image (or the green channel only, if the image is color)

figure(1); imagesc(I); colormap(gray(256)); % colorbar;

%  Filter the image with N_THETA orientations, 0, 180/N_THETA, 
%  2*(180/N_THETA),  ... (N_THETA-1)*180/N_THETA  degrees.
%
N_THETA = 8;     %  Do not change this

k = 32;     %  peak frequency  

%display(['wavelength is ' num2str(N/k)]);

thetaRange = pi/N_THETA*(0:N_THETA-1);

peakTheta   = zeros(N,N);
maxResponse = zeros(N,N);
minResponse = 10000*ones(N,N);

%  Hint:  I suggest you use the following trick to find the max
%         response (and other required quantities).
%
%     for each theta
%        filter the image with a Gabor tuned to that theta
%  Matlab-->   mask = (filterResponse > maxResponse);
%  Matlab-->   maxResponse = mask .* filterResponse + ~mask .* maxResponse
%     end

%---------------  BEGIN SOLUTION ------------------
Gabors = cell(size(thetaRange));
for i = 1:size(thetaRange,2)
   theta = thetaRange(i);
   [cosG ,sinG] = make2DGabor(N,k*sin(theta),k*cos(theta));
   Gabors{i} = {cosG + 1i * sinG, imcrop(cosG,[240 240 39 39])};
end




for i = 1:size(thetaRange,2)
   %gabFFT = abs(Gabors{i}{2});
   %gabFFT = abs(fftshift(fft2(Gabors{i}{1})));
   gabFFT = abs(fftshift(fft2(Gabors{i}{1})));
   maxVal = max(gabFFT(:));
   gabFFT(N/2 -1 : N/2 + 1,:) = maxVal;
   gabFFT(:,N/2 - 1 : N/2 + 1) = maxVal;
   inset = Gabors{i}{2};
   maxIns = max(inset(:));
   gabFFT(1:40,1:40) = inset * (maxVal/maxIns);
   % add cosine inset
   subplot(2,4,i);
   imagesc(gabFFT);
   %hold on;
   %imagesc([0.5 0.5],[0.5 0.5],Gabors{i}{2})
   colormap gray;
   axis off;
end


%Q1b

readImage;

%Redefine filters to be smaller
Gabors = cell(size(thetaRange));
N = 512;
k = 36;
for i = 1:size(thetaRange,2)
   theta = thetaRange(i);
   [cosG ,sinG] = make2DGabor(N,k*sin(theta),k*cos(theta));
   Gabors{i} = cosG + 1i * sinG;
end


filteredImgs = cell(size(thetaRange));
for i = 1:size(thetaRange,2)
    gab = Gabors{i};
    filtered = ifft2( fft2( fftshift( gab ) ) .* fft2( I ) );
    filteredImgs{i} = filtered;
end


% Find max,min

for i = 1:size(thetaRange,2)
   filtered =  abs(filteredImgs{i});
   mask = (filtered > maxResponse);
   maxResponse = mask .* filtered + ~mask .* maxResponse;
   
   theta = thetaRange(i);
   peakTheta(mask) = theta*180/pi;
   
   mask = (filtered < minResponse);
   minResponse = mask .* filtered + ~mask .* minResponse;
end


%---------------  END SOLUTION ------------------

figure
%
hsvImage = zeros(N,N,3);
%  hue is orientation
hsvImage(:,:,1) = peakTheta/180;  
%  saturation indicates whether there is a big difference between 
%  max and min
hsvImage(:,:,2) = (maxResponse-minResponse)./(maxResponse + minResponse);  % 
%  value indicates whether the response is large compared to the max
%  response over the image
hsvImage(:,:,3) = maxResponse/max(maxResponse(:));
image(hsv2rgb(hsvImage));
axis square

%print('annulusCrossSolution', '-djpeg ')
print('q1b', '-djpeg ')

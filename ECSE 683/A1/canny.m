function [ edges ] = canny( I, name )
% CANNY Canny edge detector
% Michael Smith / 260481943

% Define gaussian of 3x3 with sigma = 3 normally
%gauss = fspecial('gauss',[3 3], 5);

% For SNR test
gauss = fspecial('gauss',[3 3], 5);

% Smooth...
resultImg = conv2(double(I),gauss,'same');

% Define gradient kernels - very simple
% From Machine Vision by Jain, Kasturi and Schunk
Gx = [-1 1; -1 1];
Gy = [ 1 1; -1 -1];

resultGradientX = conv2(resultImg,Gx,'same');
resultGradientY = conv2(resultImg,Gy,'same');

% Compute magnitude/orientation images
M = sqrt(resultGradientX.^2 + resultGradientY.^2);
Theta = atan2(resultGradientY,resultGradientX);
Theta0ToPi = Theta + pi;

display = uint8(round(M./(max(M(:))/255)));
imwrite(display,strcat(name,'_magnitude.png'));

% Nonmax suppression - calculate Zeta (Theta -> 4 quadrants)
% First, convert angle into 0-3
Zeta = zeros(size(Theta),'single');
Zeta(Theta0ToPi >= 0 & Theta0ToPi <= pi/8) = 0;
Zeta(Theta0ToPi > pi/8 & Theta0ToPi <= 3*pi/8) = 1;
Zeta(Theta0ToPi > 3*pi/8 & Theta0ToPi <= 5*pi/8) = 2;
Zeta(Theta0ToPi > 5*pi/8 & Theta0ToPi <= 7*pi/8) = 3;
Zeta(Theta0ToPi > 7*pi/8 & Theta0ToPi <= 9*pi/8) = 0;
Zeta(Theta0ToPi > 9*pi/8 & Theta0ToPi <= 11*pi/8) = 1;
Zeta(Theta0ToPi > 11*pi/8 & Theta0ToPi <= 13*pi/8) = 2;
Zeta(Theta0ToPi > 13*pi/8 & Theta0ToPi <= 15*pi/8) = 3;
Zeta(Theta0ToPi > 15*pi/8 & Theta0ToPi <= 16*pi/8) = 0;

% Grow M, Zeta by 1 pixel row and column
M = [zeros(size(M,1),1) M zeros(size(M,1),1)];
M = [zeros(1,size(M,2)); M; zeros(1,size(M,2))];

Zeta = [zeros(size(Zeta,1),1) Zeta zeros(size(Zeta,1),1)];
Zeta = [zeros(1,size(Zeta,2)); Zeta; zeros(1,size(Zeta,2))];


imgSize = size(M);

N = zeros(imgSize,'single');

% Non max suppression
for i = 2:imgSize(2)-1
    for j = 2:imgSize(1)-1
        if Zeta(j,i) == 0
            neighbours = [M(j,i-1) M(j,i+1)];
        elseif Zeta(j,i) == 1
            neighbours = [M(j+1,i+1) M(j-1,i-1)];
        elseif Zeta(j,i) == 2
            neighbours = [M(j+1,i) M(j-1,i)];
        elseif Zeta(j,i) == 3
            neighbours = [M(j-1,i+1) M(j+1,i-1)];
        end
        
        if ((M(j,i) > neighbours(1)) && (M(j,i) > neighbours(2)))
            N(j,i) = M(j,i);
        end
    end
end





% Threshold w/ T1 and T2

% Note that the extra edge pixels are left in N although we set them to 0
% Normally we'd set only the outer edge but since this is a gradient we set
% the first two rows, last two rows, first two columns and last two columns
% to 0
N(1:2,:) = 0; N(end-1:end,:) = 0; N(:,1:2) = 0; N(:,end-1:end) = 0;
% Then scale N to 255 range
N = uint8(round(N * (255/max(N(:)))));

imwrite(N,strcat(name,'_nonmax.png'));

% Normally......
%T2 = 1/6;
%T1 = T2/2;

% Threshold special to SNR test
T2 = 0.655;
T1 = 0.3;

I_T1 = im2bw(N,T1);
I_T2 = im2bw(N,T2);


% Initialize a cell array to all 1 to represent unvisited pixels
% Iterate over thresholded image T2, mark pixel as visited
% Only take action if pixel = 1
% If 1, get direction
% Use direction to find neighbours
% See if neighbours do NOT exist in T2
% If NO values in T2, see if T1 has a value in either neighbour pixel
% If it does, import those 1 or 2 pixels into T2
% Mark those pixels as UNvisited
% Continue to next T2 pixel
% Loop entire thing over until sum of unvisited = 0

imgSize = size(N);

unvisited = ones(imgSize,'uint8');
while sum(unvisited(:)) ~= 0
    for i = 1:imgSize(2)
        for j = 1:imgSize(1)
            % Pixel is unvisited; visit
            if unvisited(j,i) == 1
                unvisited(j,i) = 0;
                % If pixels is an edge in T2...
                if I_T2(j,i) == 1
                    % Get neighbours - note that we go along gradient not
                    % perpendicular to it as before
                    if Zeta(j,i) == 0
                        neighbours = {[j+1,i],[j-1,i]};
                        neighboursT2 = [I_T2(j+1,i) I_T2(j-1,i)];
                        neighboursT1 = [I_T1(j+1,i) I_T1(j-1,i)];
                    elseif Zeta(j,i) == 1
                        neighbours = {[j-1,i+1],[j+1,i-1]};
                        neighboursT2 = [I_T2(j-1,i+1) I_T2(j+1,i-1)];
                        neighboursT1 = [I_T1(j-1,i+1) I_T1(j+1,i-1)];
                    elseif Zeta(j,i) == 2
                        neighbours = {[j,i-1],[j,i+1]};
                        neighboursT2 = [I_T2(j,i-1) I_T2(j,i+1)];
                        neighboursT1 = [I_T1(j,i-1) I_T1(j,i+1)];
                    elseif Zeta(j,i) == 3
                        neighbours = {[j+1,i+1],[j-1,i-1]};
                        neighboursT2 = [I_T2(j+1,i+1) I_T2(j-1,i-1)];
                        neighboursT1 = [I_T1(j+1,i+1) I_T1(j-1,i-1)];
                    end
                    
                    % If neighbours aren't in T2....
                    if neighboursT2(1) == 0
                        if neighboursT1(1) ~= 0
                            % Add from T1
                            I_T2(neighbours{1}(1),neighbours{1}(2)) = 1;
                            % Mark as unvisited
                            unvisited(neighbours{1}(1),neighbours{1}(2)) = 1;
                        end
                    end
                    if neighboursT2(2) == 0
                        if neighboursT1(2) ~= 0
                            % Add from T1
                            I_T2(neighbours{2}(1),neighbours{2}(2)) = 1;
                            % Mark as unvisited
                            unvisited(neighbours{2}(1),neighbours{2}(2)) = 1;
                        end
                    end
                    
                end
            end
        end
    end
end

% Remove excess pixels
I_T2 = I_T2(2:end-1,2:end-1,:);

edges = I_T2;

imwrite(edges,strcat(name,'_final.png'));
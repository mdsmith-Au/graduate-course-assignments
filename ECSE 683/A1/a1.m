function [ output_args ] = a1( I )
% Corner detector
% Michael Smith / 260481943

% Define derivative of gaussian operators in x and y
sigma = 2;
gauss=fspecial('gauss',[5 5], sigma);
[Gx,Gy] = gradient(gauss);   
%[Gxx,Gxy] = gradient(Gx);
%[Gyx,Gyy] = gradient(Gy);

% apply operators
filterX = conv2(double(I),Gx,'same');
filterY = conv2(double(I),Gy,'same');

imgSize = size(filterX);

%%% Threshold used to eliminate poor corners
T = 1e+3;

cornersIdx = 1;

% For every point p in a 5x5 neighbourhood Q
% Note that we ignore the edge pixels - we don't search for corners there
for i = 3:(imgSize(2) - 2)
    for j = 3:(imgSize(1) - 2)
        % Get derivatives in X and Y
        filtXN = filterX(j-2:j+2,i-2:i+2);
        filtXN = filtXN(:);
        filtYN = filterY(j-2:j+2,i-2:i+2);
        filtYN = filtYN(:);
        % Compute matrix C
        C = [ sum(filtXN.^2), sum(filtXN .* filtYN); sum(filtYN .* filtXN), sum(filtYN.^2) ];
        % Calculate eigenvalues
        e = eig(C);

        % If smaller eigenvalue is above threshold add to corner "list"
        if e(1) > T
            corners(cornersIdx,:) = [ e(1) j i ];
            cornersIdx = cornersIdx + 1;
        end
    end
end

% Sort list
corners = sortrows(corners,1);

cornersImg = zeros(imgSize,'uint8');
finalCornerList = zeros(1,2,'double');
cornerListIndex = 1;

% Non-maximum suppression parameter that eliminates other corners near a
% detected corner
windowSizeForCornerNMS = 8; % on each side of pixel, so 2 = 5x5

% Go over all corners....
for i = size(corners,1):-1:1
    % If corner actually exists (not removed from list)
    if corners(i,1) > 0
        jCol = corners(:,2);
        iCol = corners(:,3);
        
        y = corners(i,2);
        x = corners(i,3);
        
        % Add corner to list and an image
        cornersImg(y,x) = 255;
        finalCornerList(cornerListIndex,:) = [x y];
        cornerListIndex = cornerListIndex + 1;
        
        minY = y - windowSizeForCornerNMS;
        maxY = y + windowSizeForCornerNMS;
        minX = x - windowSizeForCornerNMS;
        maxX = x + windowSizeForCornerNMS;
        
        % Find and remove any other corners in neighbourhood
        % Also add this (good) corner to our final list
        % First - search J column
        for k = i-1:-1:1
           if (jCol(k) >= minY) && (jCol(k) <= maxY)
               % Now check I value
               if (iCol(k) >= minX) && (iCol(k) <= maxX)
                  % Remove!
                  corners(k,:) = [0 0 0]; 
               end
           end
        end
    end
end

% Image showing corner positions in space
cornersImg = cornersImg(2:end-1,2:end-1,:);
imshow(cornersImg);

% Real corner values for 10 corners picked manually
truth = [ 42 41; 68 41; 92 66; 118 91; 143 116; 168 141; 193 167; 218 167; 218 116; 194 90];

% Compare with truth for checkerboard
dist = 0;
counter = 0;
for i = 1:size(truth,1)
    point = truth(i,:);
    replicated = repmat(point,size(finalCornerList,1),1);
    distanceXY = finalCornerList - replicated;
    distance = sqrt(distanceXY(:,1).^2 + distanceXY(:,2).^2);
    minDist = min(distance);
    if (minDist < 10)
        dist = dist + minDist;
        counter = counter + 1;
    end
end

dist = dist/counter;
fprintf('%i matches found. Average: %f\n',counter,dist);

% For real image...plot points
figure; imshow(I);
hold on;
plot(finalCornerList(:,1), finalCornerList(:,2), 'r*');
set(gca,'position',[0 0 1 1],'units','normalized')
axis off;

function [  ] = prob3_lsq(  )
% Problem 3 Least Squares solution
% Michael Smith / 260481943

load('problem3_angel_zdepth');
%angel = rgb2gray(imread('problem3_angel-deg0.00.ppm'));

sigma = 1.5;
gauss=fspecial('gauss',[5 5], sigma);
z_depth = conv2(z_depth, gauss, 'same');

% least squares - A is computed from x/y, so is constant 
A = [4,4,4,-2,-2,1;4,2,1,-2,-1,1;4,0,0,-2,0,1;4,-2,1,-2,1,1;4,-4,4,-2,2,1;1,2,4,-1,-2,1;1,1,1,-1,-1,1;1,0,0,-1,0,1;1,-1,1,-1,1,1;1,-2,4,-1,2,1;0,0,4,0,-2,1;0,0,1,0,-1,1;0,0,0,0,0,1;0,0,1,0,1,1;0,0,4,0,2,1;1,-2,4,1,-2,1;1,-1,1,1,-1,1;1,0,0,1,0,1;1,1,1,1,1,1;1,2,4,1,2,1;4,-4,4,2,-2,1;4,-2,1,2,-1,1;4,0,0,2,0,1;4,2,1,2,1,1;4,4,4,2,2,1];

% Do (A^T * A)^-1 * A^T on a 5x5 window across the image
% That is, (A^T * A)^-1 * A^T * b = our constants
convEq = (A'*A)^-1 * A';
convEq1 = reshape(convEq(1,:), [5 5]);
convEq2 = reshape(convEq(2,:), [5 5]);
convEq3 = reshape(convEq(3,:), [5 5]);
convEq4 = reshape(convEq(4,:), [5 5]);
convEq5 = reshape(convEq(5,:), [5 5]);

% use convolution to get a,b,c,d etc. - constants
% We need a-e but not f
a = conv2(z_depth,convEq1,'same');
b = conv2(z_depth,convEq2,'same');
c = conv2(z_depth,convEq3,'same');
d = conv2(z_depth,convEq4,'same');
e = conv2(z_depth,convEq5,'same');

% Calculate derivatives from constants
dX = 2*a + b + d;
dY = b  + 2*c + e;
dXX = 2*a;
dYY = 2*c;
dXY = b;

imgSize = size(z_depth);    

H = zeros(imgSize,'double');
K = zeros(imgSize,'double');

%Calc H/K
for i = 1:imgSize(2)
    for j = 1:imgSize(1)
        K(j,i) = (dXX(j,i) * dYY(j,i) - dXY(j,i)^2)/((1+dX(j,i)^2 + dY(j,i)^2)^2);
        H(j,i) = ((1+dX(j,i)^2)*dYY(j,i) - 2*dX(j,i)*dY(j,i)*dXY(j,i) + (1 + dY(j,i)^2)*dXX(j,i))/(2*(1 + dX(j,i)^2 + dY(j,i)^2)^(3.0/2.0));
    end
end

H(abs(H) < 1e-4) = 0;
K(abs(K) < 1e-4) = 0;

% Categorize into shapes
S = zeros([imgSize(1), imgSize(2), 3],'uint8');
for i = 1:imgSize(2)
    for j = 1:imgSize(1)
        % Plane - black
        if K(j,i) == 0 && H(j,i) == 0
            S(j,i,:) = [0 0 0];
        % Concave cylindrical - red
        elseif K(j,i) == 0 && H(j,i) > 0
            S(j,i,:) = [ 255 0 0];
        % Convex cylindrical - green
        elseif K(j,i) == 0 && H(j,i) < 0
            S(j,i,:) = [0 255 0];
        % Concave elliptic - blue
        elseif K(j,i) > 0 && H(j,i) > 0
            S(j,i,:) = [ 0 0 255];
        % Convex elliptic - yellow
        elseif K(j,i) > 0 && H(j,i) < 0
            S(j,i,:) = [ 255 255 0 ];
        % Hyperbolic - Cyan
        elseif K(j,i) < 0
            S(j,i,:) = [ 0 255 255];
        end
    end
end

imshow(S);
end


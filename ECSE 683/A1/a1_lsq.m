function [ output_args ] = a1_lsq( I )
% Corner detector with least squares
% Michael Smith / 260481943

% Approx with a*x^2 + b*xy + c*y^2 + d*x + e*y + f
% b = values of intensity
% A*x = b

imgSize = size(I);

sigma = 1.5;
gauss=fspecial('gauss',[5 5], sigma);
I2 = conv2(double(I),gauss,'same');

% least squares - A is computed from x/y, so is constant
A = [4,4,4,-2,-2,1;4,2,1,-2,-1,1;4,0,0,-2,0,1;4,-2,1,-2,1,1;4,-4,4,-2,2,1;1,2,4,-1,-2,1;1,1,1,-1,-1,1;1,0,0,-1,0,1;1,-1,1,-1,1,1;1,-2,4,-1,2,1;0,0,4,0,-2,1;0,0,1,0,-1,1;0,0,0,0,0,1;0,0,1,0,1,1;0,0,4,0,2,1;1,-2,4,1,-2,1;1,-1,1,1,-1,1;1,0,0,1,0,1;1,1,1,1,1,1;1,2,4,1,2,1;4,-4,4,2,-2,1;4,-2,1,2,-1,1;4,0,0,2,0,1;4,2,1,2,1,1;4,4,4,2,2,1];

% Do (A^T * A)^-1 * A^T on a 5x5 window across the image
% That is, (A^T * A)^-1 * A^T * b = our constants
convEq = (A'*A)^-1 * A';
convEq1 = reshape(convEq(1,:), [5 5]);
convEq2 = reshape(convEq(2,:), [5 5]);
convEq3 = reshape(convEq(3,:), [5 5]);
convEq4 = reshape(convEq(4,:), [5 5]);
convEq5 = reshape(convEq(5,:), [5 5]);

% use convolution to get a,b,c,d etc. - constants
% We need a-e but not f
a = conv2(I2,convEq1,'same');
b = conv2(I2,convEq2,'same');
c = conv2(I2,convEq3,'same');
d = conv2(I2,convEq4,'same');
e = conv2(I2,convEq5,'same');


% Make image for testing
% filterX = zeros(imgSize);
% filterY = zeros(imgSize);
%  for i = 1:imgSize(2)
%      for j = 1:imgSize(1)
%         filterX(j,i) = 2*a(j,i) + b(j,i) + d(j,i);
%         filterY(j,i) = b(j,i) + 2*c(j,i) + e(j,i);
%      end
%  end

%%% Threshold used to eliminate poor corners
T = 1e+3;

cornersIdx = 1;

% For every point p in a 5x5 neighbourhood Q
for i = 3:(imgSize(2) - 2)
    for j = 3:(imgSize(1) - 2)
        % Get derivatives in X and Y
        % Derivative w.r.t. x
        % 2*a*x + b*y + d
        % w.r.t. y
        % b*x + 2*c*y + e
        filtXN = 2*a(j-2:j+2,i-2:i+2) + b(j-2:j+2,i-2:i+2) + d(j-2:j+2,i-2:i+2);
        filtXN = filtXN(:);
        filtYN = b(j-2:j+2,i-2:i+2) + 2*c(j-2:j+2,i-2:i+2) + e(j-2:j+2,i-2:i+2);
        filtYN = filtYN(:);
        % Compute matrix C
        C = [ sum(filtXN.^2), sum(filtXN .* filtYN); sum(filtYN .* filtXN), sum(filtYN.^2) ];
        % Calculate eigenvalues
        eigen = eig(C);

        % If smaller eigenvalue is above threshold add to corner "list"
        % Rank 2 check is probably redundant since lamda1 = 0 (or e-18 or
        % some such) anyway
        if rank(C) == 2 && eigen(1) > T
            corners(cornersIdx,:) = [ eigen(1) j i ];
            cornersIdx = cornersIdx + 1;
        end
    end
end

% Sort list
corners = sortrows(corners,1);

cornersImg = zeros(imgSize,'uint8');
finalCornerList = zeros(1,2,'double');
cornerListIndex = 1;

% Non-maximum suppression parameter that eliminates other corners near a
% detected corner
windowSizeForCornerNMS = 8; % on each side of pixel, so 2 = 5x5

% Go over all corners....
for i = size(corners,1):-1:1
    % If corner actually exists (not removed from list)
    if corners(i,1) > 0
        jCol = corners(:,2);
        iCol = corners(:,3);
        
        y = corners(i,2);
        x = corners(i,3);
        
        finalCornerList(cornerListIndex,:) = [x y];
        cornerListIndex = cornerListIndex + 1;
        cornersImg(y,x) = 255;
        
        minY = y - windowSizeForCornerNMS;
        maxY = y + windowSizeForCornerNMS;
        minX = x - windowSizeForCornerNMS;
        maxX = x + windowSizeForCornerNMS;
        
        % Find and remove any other corners in neighbourhood
        % First - search J column
        for k = i-1:-1:1
           if (jCol(k) >= minY) && (jCol(k) <= maxY)
               % Now check I value
               if (iCol(k) >= minX) && (iCol(k) <= maxX)
                  % Remove!
                  corners(k,:) = [0 0 0]; 
               end
           end
        end
    end
end

% Image showing corner positions in space
cornersImg = cornersImg(2:end-1,2:end-1,:);
imshow(cornersImg);

% Real corner values for 10 corners picked manually
truth = [ 42 41; 68 41; 92 66; 118 91; 143 116; 168 141; 193 167; 218 167; 218 116; 194 90];

% Compare with truth for checkerboard
dist = 0;
counter = 0;
for i = 1:size(truth,1)
    point = truth(i,:);
    replicated = repmat(point,size(finalCornerList,1),1);
    distanceXY = finalCornerList - replicated;
    distance = sqrt(distanceXY(:,1).^2 + distanceXY(:,2).^2);
    minDist = min(distance);
    if (minDist < 10)
        dist = dist + minDist;
        counter = counter + 1;
    end
end

dist = dist/counter;
fprintf('%i matches found. Average: %f\n',counter,dist);

% For real image...plot points
figure; imshow(I);
hold on;
plot(finalCornerList(:,1), finalCornerList(:,2), 'r*');
set(gca,'position',[0 0 1 1],'units','normalized')
axis off;
function [  ] = prob3(  )
% Problem 3 discrete derivative
% Michael Smith / 260481943

load('problem3_angel_zdepth');
%angel = rgb2gray(imread('problem3_angel-deg0.00.ppm'));

sigma = 1.5;
gauss=fspecial('gauss',[5 5], sigma);
[Gx,Gy] = gradient(gauss);   
[Gxx,Gxy] = gradient(Gx);
[~,Gyy] = gradient(Gy);

% Calc derivatives
dX = conv2(z_depth,Gx,'same');
dY = conv2(z_depth,Gy,'same');
dXX = conv2(z_depth,Gxx,'same');
dYY = conv2(z_depth,Gyy,'same');
dXY = conv2(z_depth,Gxy,'same');

imgSize = size(z_depth);

H = zeros(imgSize,'double');
K = zeros(imgSize,'double');

%Calc H/K
for i = 1:imgSize(2)
    for j = 1:imgSize(1)
        K(j,i) = (dXX(j,i) * dYY(j,i) - dXY(j,i)^2)/((1+dX(j,i)^2 + dY(j,i)^2)^2);
        H(j,i) = ((1+dX(j,i)^2)*dYY(j,i) - 2*dX(j,i)*dY(j,i)*dXY(j,i) + (1 + dY(j,i)^2)*dXX(j,i))/(2*(1 + dX(j,i)^2 + dY(j,i)^2)^(3.0/2.0));
    end
end

H(abs(H) < 1e-4) = 0;
K(abs(K) < 1e-4) = 0;

% Categorize into shapes
% Could be done with cross product but this is a little more intuitive and
% easier to directly map from T&V
S = zeros([imgSize(1), imgSize(2), 3],'uint8');
for i = 1:imgSize(2)
    for j = 1:imgSize(1)
        % Plane - black
        if K(j,i) == 0 && H(j,i) == 0
            S(j,i,:) = [0 0 0];
        % Concave cylindrical - red
        elseif K(j,i) == 0 && H(j,i) > 0
            S(j,i,:) = [ 255 0 0];
        % Convex cylindrical - green
        elseif K(j,i) == 0 && H(j,i) < 0
            S(j,i,:) = [0 255 0];
        % Concave elliptic - blue
        elseif K(j,i) > 0 && H(j,i) > 0
            S(j,i,:) = [ 0 0 255];
        % Convex elliptic - yellow
        elseif K(j,i) > 0 && H(j,i) < 0
            S(j,i,:) = [ 255 255 0 ];
        % Hyperbolic - Cyan
        elseif K(j,i) < 0
            S(j,i,:) = [ 0 255 255];
        end
    end
end

imshow(S);
end


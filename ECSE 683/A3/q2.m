function [  ] = q2(  )

% Read images
[images,imgSize] = read_images();

% Find SURF features (in all images although not needed )
[matches] = getFeatures(images);

% Match features - FEATURE_POINT_MATCHING
[dMatrix, pointMatrix] = feature_point_matching(images,matches);

% Calculate W matrix - consists of the same X,Y features across all images
W = [];
for img = 1:size(pointMatrix,1)
    points = pointMatrix{img};
    xPts = points(:,1);
    yPts = points(:,2);
    
    meanX = mean(xPts);
    meanY = mean(yPts);
    
    W(img,:) = xPts' - meanX;
    W(size(pointMatrix,1) + img,:) = yPts' - meanY;
    
end

%motstructfromfeats would be called here if it worked...

% xdata = pointMatrix{4}(:,1);
% ydata = pointMatrix{4}(:,2);
% zdata = dMatrix{4}(:,1);
% 
% F = scatteredInterpolant(xdata,ydata,zdata,'nearest');
% 
% ti = 0 : 1 : 200;
% [xq,yq] = meshgrid(ti,ti);
% vq = F(xq,yq);
% 
% imagesc(vq);
end


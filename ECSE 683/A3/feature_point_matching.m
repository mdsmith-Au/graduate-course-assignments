function [ dMatrix,pointMatrix ] = feature_point_matching( images,matches )
%FEATURE_POINT_MATCHING
% Smooth images for gradient calculations
filtered = imgaussfilt3(images,1.5);

% Calculate gradient for all images, space + time
[dX,dY,dT] = gradient(filtered);

% Pad image 
n = 5;
padAmount = round(n/2 - 1);
dX = padarray(dX, [padAmount padAmount],'replicate');
dY = padarray(dY, [padAmount padAmount],'replicate');
dT = padarray(dT, [padAmount padAmount],'replicate');

%threshold  = 10000;

% Define disparity matrix and another one for point locations
dMatrix = cell([size(images,3)-1,1]);
pointMatrix = cell([size(images,3),1]);

pointMatrix{1} = matches{1,1}.Location;

for img = 1 : size(images,3)-1
    
    % Matrix containing image points
    p1 = pointMatrix{img};
    
    image1 = images(:,:,img);
    image2 = images(:,:,img+1);
    
    % define disparity matrix, point matrix
    dEntry = zeros([size(p1,1), 2]);
    newPts = zeros([size(p1,1), 2]);
    
    for ptNum = 1 : size(p1,1)
        cont = 1;
        
        x_orig = p1(ptNum,1);
        y_orig = p1(ptNum,2);

        d = 0;
        prevssd = 0;
        
        while(cont)
            
            x = round(x_orig);
            y = round(y_orig);       

            % Calculate displacement from 1->2
            % Step 1
            Xpatch = dX(x - padAmount:x+padAmount, y-padAmount:y+padAmount,img);
            Ypatch = dY(x - padAmount:x+padAmount, y-padAmount:y+padAmount,img);
            Tpatch = dT(x - padAmount:x+padAmount, y-padAmount:y+padAmount,img);

            A = [reshape(Xpatch,n^2,1), reshape(Ypatch,n^2,1)];
            b = -reshape(Tpatch,n^2,1);
            v = (A'*A)\A' * b;

            d = d + v;

            % Calculate Q 
            xPrime = round(x_orig + v(1));
            yPrime = round(y_orig + v(2));

            Q = image1( y-padAmount:y+padAmount,x - padAmount:x+padAmount);
            

            % Calculate Q2 using image 2
            
            % Get out of here if we're trying to track features off the
            % image
            if (yPrime - padAmount <= 0 || yPrime + padAmount >= size(image2,1) || xPrime - padAmount <=0 || xPrime + padAmount >= size(image2,2))
                cont = 0;
                break;
            end
            Q2 = image2( yPrime-padAmount:yPrime+padAmount, xPrime - padAmount:xPrime+padAmount);

            % Calculate SSD between image 2 and image 1 using what should
            % be an equivalent patch in both.  T&V seems to contradict
            % himself between the paragraph on page 198 and the actual
            % algorithm box so I treated this as a simple matching problem
            tmp = (Q2 - Q).^2;
            ssd = sum(tmp(:));
            
            % Note: T&V use threshold as a way of exiting, but I noticed that
            % the SSD tends to go down to a low value then increase
            % as d increases, and a threshold will often fail to capture
            % the d with the lowest SSD
            % This comparison with "prevssd" fixes this; we don't use a
            % threshold
            if (prevssd == 0 || ssd < prevssd)
                prevssd = ssd;
                newPts(ptNum,:) = [x_orig + v(1), y_orig + v(2)];
                x_orig = x_orig + v(1);
                y_orig = y_orig + v(2);
                dEntry(ptNum,:) = d;
            elseif  (ssd >= prevssd)
                cont = 0;
            end
        end
    end
    dMatrix{img} = dEntry;
    pointMatrix{img+1} = newPts;
end


end


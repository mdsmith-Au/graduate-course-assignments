function [ images,imgSize ] = read_images(  )
%READ_IMAGES 
pepsi = imageSet('pepsi');


imgSize = size(rgb2gray(pepsi.read(1)));
images = zeros([imgSize pepsi.Count]);
for i = 1 : pepsi.Count
    % Read in image 
   images(:,:,i) = rgb2gray(read(pepsi,i)); 
end

end


function [ output_args ] = motstructfromfeats( W )
%MOTSTRUCTFROMFEATS Summary of this function goes here
%   Detailed explanation goes here

[u,d,v] = svd(W);

% Set to 0 all but 3 largest singular values in D
diag11 = d(1,1);
diag22 = d(2,2);
diag33 = d(3,3);

d(logical(eye(size(d)))) = 0;
d(1,1) = diag11;
d(2,2) = diag22;
d(3,3) = diag33;

% Calculate d prime,u prime and v prime
dprime = d(1:3,1:3);
uprime = u(1:size(u,1),1:3);
vprime = v(1:size(v,1),1:3);

% Calculate R hat and S hat
R_hat = uprime*(dprime)^(1/2);
S_hat = (dprime^(1/2))*vprime';

% here we would calculate Q, but this doesn't quite work...
% Newton's method could be used if I knew how to wrangle my equations into
% a format that I could actually put to work
%i_rows = R_hat(1:3,:);
%j_rows = R_hat(20:22,:);

% syms a b c d e f g h k
% Q = [a b c; d e f; g h k];
% 
% eqn1 = (i_rows' * Q * transpose(Q) * i_rows == 1);
% eqn2 = (j_rows' * Q * transpose(Q) * j_rows == 1);
% eqn3 = (i_rows' * Q * transpose(Q) * j_rows == 0);

%S = vpasolve([eqn1(:); eqn2(:); eqn3(:)],[a b c d e f g h k]);
end


function [ matches ] = getFeatures( images )
%GETFEATURES Finds SURF features in all images
images = uint8(images);

points = cell(size(images,3));
for i = 1 : size(images,3)
    points{i} = detectSURFFeatures(images(:,:,i));
end

features = cell([size(images,3), 2]);
for i = 1 : size(images,3)
    [features{i,1}, features{i,2}] = extractFeatures(images(:,:,i),points{i});
end

matches = cell([size(images,3) - 1, 2]);
for i = 1 : size(images,3)-1
    indexPairs = matchFeatures(features{i,1}, features{i+1,1});
    vpts1 = features{i,2};
    vpts2 = features{i+1,2};
    matches{i,1} = vpts1(indexPairs(:,1));
    matches{i,2} = vpts2(indexPairs(:,2));
end


end


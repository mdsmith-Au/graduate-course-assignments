function [  ] = constant_flow(  )
% Constant flow - T&V

[images,imgSize] = read_images();

[imgSize pepsi.Count]);

% Blur both spatially and temporall with sigma = 1.5
% imgaussfilt3 is intended for 3d images but we structured the matrix
% earlier in such a way that the Z dimension = time so this function can be
% used
filtered = imgaussfilt3(images,1.5);


% Calculate disparity for all images, space + time
[disparityX,disparityY,disparityT] = gradient(filtered);

% Pad image 
n = 5;
padAmount = round(n/2 - 1);
disparityX = padarray(disparityX, [padAmount padAmount],'replicate');
disparityY = padarray(disparityY, [padAmount padAmount],'replicate');
disparityT = padarray(disparityT, [padAmount padAmount],'replicate');

opticalFlow = zeros(imgSize(1),imgSize(2),pepsi.Count,2);
for img = 1 : pepsi.Count
   for x = padAmount+1 : imgSize(2)+padAmount
       for y = padAmount+1 : imgSize(1)+padAmount
           % Get gradient in X, Y and time
            Xpatch = disparityX(x - padAmount:x+padAmount, y-padAmount:y+padAmount,img);
            Ypatch = disparityY(x - padAmount:x+padAmount, y-padAmount:y+padAmount,img);
            Tpatch = disparityT(x - padAmount:x+padAmount, y-padAmount:y+padAmount,img);
            
            % Calculate least squares approx.
            A = [reshape(Xpatch,n^2,1), reshape(Ypatch,n^2,1)];
            b = -reshape(Tpatch,n^2,1);
            v = (A'*A)\A' * b;
            % Store result
            opticalFlow(x-padAmount,y-padAmount,img,:) = v;
       end
   end
end

%Plot flow field
%quiver(opticalFlow(:,:,4,1),opticalFlow(:,:,4,2),2);

%Sparse flow field
opt1 = opticalFlow(:,:,4,1);
opt2 = opticalFlow(:,:,4,2);
subm1 = opt1(1:5:end,1:5:end);
subm2 = opt2(1:5:end,1:5:end);
quiver(subm1,subm2);

% At depth of can, 6.35cm = 65.68px
pxPercm = 65.68/6.35;

% We get flow in pixels, so multiply by that scale factor to get depth
% This isn't quite right - needs more work
depthMap = pxPercm * opticalFlow(:,:,4,1);

end



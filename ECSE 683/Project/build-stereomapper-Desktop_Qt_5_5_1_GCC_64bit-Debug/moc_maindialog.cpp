/****************************************************************************
** Meta object code from reading C++ file 'maindialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../stereomapper/maindialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'maindialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainDialog_t {
    QByteArrayData data[24];
    char stringdata0[631];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainDialog_t qt_meta_stringdata_MainDialog = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainDialog"
QT_MOC_LITERAL(1, 11, 30), // "on_shutterSpinBox_valueChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 32), // "on_resizeSmallPushButton_clicked"
QT_MOC_LITERAL(4, 76, 24), // "on_whiteCheckBox_clicked"
QT_MOC_LITERAL(5, 101, 23), // "on_gridCheckBox_clicked"
QT_MOC_LITERAL(6, 125, 32), // "on_recordPosesPushButton_clicked"
QT_MOC_LITERAL(7, 158, 30), // "on_playPosesPushButton_clicked"
QT_MOC_LITERAL(8, 189, 31), // "on_deletePosePushButton_clicked"
QT_MOC_LITERAL(9, 221, 28), // "on_addPosePushButton_clicked"
QT_MOC_LITERAL(10, 250, 27), // "on_resizePushButton_clicked"
QT_MOC_LITERAL(11, 278, 30), // "on_showCamerasCheckBox_clicked"
QT_MOC_LITERAL(12, 309, 33), // "on_backgroundWallCheckBox_cli..."
QT_MOC_LITERAL(13, 343, 35), // "on_backgroundWallSlider_slide..."
QT_MOC_LITERAL(14, 379, 8), // "position"
QT_MOC_LITERAL(15, 388, 25), // "on_resetBusButton_clicked"
QT_MOC_LITERAL(16, 414, 32), // "on_readFromFilesCheckBox_clicked"
QT_MOC_LITERAL(17, 447, 27), // "on_stereoScanButton_clicked"
QT_MOC_LITERAL(18, 475, 21), // "on_exitButton_clicked"
QT_MOC_LITERAL(19, 497, 30), // "on_stopCapturingButton_clicked"
QT_MOC_LITERAL(20, 528, 36), // "on_captureFromFirewireButton_..."
QT_MOC_LITERAL(21, 565, 21), // "newStereoImageArrived"
QT_MOC_LITERAL(22, 587, 20), // "newHomographyArrived"
QT_MOC_LITERAL(23, 608, 22) // "newDisparityMapArrived"

    },
    "MainDialog\0on_shutterSpinBox_valueChanged\0"
    "\0on_resizeSmallPushButton_clicked\0"
    "on_whiteCheckBox_clicked\0"
    "on_gridCheckBox_clicked\0"
    "on_recordPosesPushButton_clicked\0"
    "on_playPosesPushButton_clicked\0"
    "on_deletePosePushButton_clicked\0"
    "on_addPosePushButton_clicked\0"
    "on_resizePushButton_clicked\0"
    "on_showCamerasCheckBox_clicked\0"
    "on_backgroundWallCheckBox_clicked\0"
    "on_backgroundWallSlider_sliderMoved\0"
    "position\0on_resetBusButton_clicked\0"
    "on_readFromFilesCheckBox_clicked\0"
    "on_stereoScanButton_clicked\0"
    "on_exitButton_clicked\0"
    "on_stopCapturingButton_clicked\0"
    "on_captureFromFirewireButton_clicked\0"
    "newStereoImageArrived\0newHomographyArrived\0"
    "newDisparityMapArrived"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  119,    2, 0x08 /* Private */,
       3,    0,  122,    2, 0x08 /* Private */,
       4,    0,  123,    2, 0x08 /* Private */,
       5,    0,  124,    2, 0x08 /* Private */,
       6,    0,  125,    2, 0x08 /* Private */,
       7,    0,  126,    2, 0x08 /* Private */,
       8,    0,  127,    2, 0x08 /* Private */,
       9,    0,  128,    2, 0x08 /* Private */,
      10,    0,  129,    2, 0x08 /* Private */,
      11,    0,  130,    2, 0x08 /* Private */,
      12,    0,  131,    2, 0x08 /* Private */,
      13,    1,  132,    2, 0x08 /* Private */,
      15,    0,  135,    2, 0x08 /* Private */,
      16,    0,  136,    2, 0x08 /* Private */,
      17,    0,  137,    2, 0x08 /* Private */,
      18,    0,  138,    2, 0x08 /* Private */,
      19,    0,  139,    2, 0x08 /* Private */,
      20,    0,  140,    2, 0x08 /* Private */,
      21,    0,  141,    2, 0x08 /* Private */,
      22,    0,  142,    2, 0x08 /* Private */,
      23,    0,  143,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainDialog *_t = static_cast<MainDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_shutterSpinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_resizeSmallPushButton_clicked(); break;
        case 2: _t->on_whiteCheckBox_clicked(); break;
        case 3: _t->on_gridCheckBox_clicked(); break;
        case 4: _t->on_recordPosesPushButton_clicked(); break;
        case 5: _t->on_playPosesPushButton_clicked(); break;
        case 6: _t->on_deletePosePushButton_clicked(); break;
        case 7: _t->on_addPosePushButton_clicked(); break;
        case 8: _t->on_resizePushButton_clicked(); break;
        case 9: _t->on_showCamerasCheckBox_clicked(); break;
        case 10: _t->on_backgroundWallCheckBox_clicked(); break;
        case 11: _t->on_backgroundWallSlider_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_resetBusButton_clicked(); break;
        case 13: _t->on_readFromFilesCheckBox_clicked(); break;
        case 14: _t->on_stereoScanButton_clicked(); break;
        case 15: _t->on_exitButton_clicked(); break;
        case 16: _t->on_stopCapturingButton_clicked(); break;
        case 17: _t->on_captureFromFirewireButton_clicked(); break;
        case 18: _t->newStereoImageArrived(); break;
        case 19: _t->newHomographyArrived(); break;
        case 20: _t->newDisparityMapArrived(); break;
        default: ;
        }
    }
}

const QMetaObject MainDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_MainDialog.data,
      qt_meta_data_MainDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainDialog.stringdata0))
        return static_cast<void*>(const_cast< MainDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int MainDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 21;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

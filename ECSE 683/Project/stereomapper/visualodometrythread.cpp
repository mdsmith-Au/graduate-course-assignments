#include "visualodometrythread.h"

using namespace std;
using namespace cv;

bool calibratedAlready = false;

VisualOdometryThread::VisualOdometryThread(CalibIO *calib, QObject *parent) :
QThread(parent),
calib(calib) {
    //VisualOdometryStereo::parameters param2;
    //
    //  // calibration parameters for sequence 2010_03_09_drive_0019
    //  param.calib.f  = 645.24; // focal length in pixels
    //  param.calib.cu = 635.96; // principal point (u-coordinate) in pixels
    //  param.calib.cv = 194.13; // principal point (v-coordinate) in pixels
    //  param.base     = 0.5707; // baseline in meters



    simg = 0;
    time_prev.tv_sec = 0;
    time_prev.tv_usec = 0;
    time_curr.tv_sec = 0;
    time_curr.tv_usec = 0;
    record_raw_odometry = false;
    H_total = Matrix(4, 4);
    H_total.eye();
}

VisualOdometryThread::~VisualOdometryThread() {
    if (simg != 0) {
        delete simg;
        simg = 0;
    }
    delete vo;
}

void VisualOdometryThread::pushBack(StereoImage::simage &s, bool record_raw_odometry_) {
    if (simg != 0) {
        delete simg;
        simg = 0;
    }
    simg = new StereoImage::simage(s);
    record_raw_odometry = record_raw_odometry_;
}

void VisualOdometryThread::run() {

    if (simg != 0 && simg->width > 0 && simg->height > 0) {

        if (!calibratedAlready) {
            // read calibration
            float f = 1;
            float cu = 0;
            float cv = 0;
            float b = 1;

            if (calib->calibrated()) {
                f = cvmGet(calib->P1_roi, 0, 0);
                cu = cvmGet(calib->P1_roi, 0, 2);
                cv = cvmGet(calib->P1_roi, 1, 2);
                b = -cvmGet(calib->P2_roi, 0, 3) / cvmGet(calib->P2_roi, 0, 0);

                cout << "f: " << f << endl << "cu: " << cu << endl << "cv: " << cv << endl << "b: " << b << endl;
            }

            // set most important visual odometry parameters
            // for a full parameter list, look at: viso_stereo.h
            VisualOdometryStereo::parameters param2;


            // calibration parameters for sequence 2010_03_09_drive_0019
            param2.calib.f = f; // focal length in pixels
            param2.calib.cu = cu; // principal point (u-coordinate) in pixels
            param2.calib.cv = cv; // principal point (v-coordinate) in pixels
            param2.base = b; // baseline in meters

            param2.bucket.max_features = 5;
            param2.bucket.bucket_height = 50;
            param2.bucket.bucket_width = 50;

            param2.match.nms_n = 3;
            param2.match.nms_tau = 50;
            param2.match.match_binsize = 50;
            param2.match.match_radius = 300;
            param2.match.match_disp_tolerance = 2;
            param2.match.outlier_disp_tolerance = 5;
            param2.match.outlier_flow_tolerance = 5;
            param2.match.multi_stage = 1;
            param2.match.half_resolution = 1;
            param2.match.refinement = 2;

            vo = new VisualOdometryStereo(param2);

            calibratedAlready = true;
        }


        // get time
//        time_prev = time_curr;
//        time_curr = simg->time;

//        Timer t;
//        t.start("push");
//        int dims[] = {simg->width, simg->height, simg->step};
//        matcher->pushBack(simg->I1, simg->I2, dims, false);

//        t.start("match");
//        matcher->matchFeatures(2);
//        matcher->bucketFeatures(5, 50, 50);

        // grab matches
//        matches = matcher->getMatches();

//        t.start("vo");

        // visual odometry
        //            vo->setCalibration(f, cu, cv, b);
        int32_t dims[] = {simg->width, simg->height, simg->width};

        vo->process(simg->I1,simg->I2,dims );

        Matrix H_inv = Matrix::eye(4);
        Matrix H = vo->getMotion ();

//        cout << "Matches:" << vo->getNumberOfMatches() << endl << "Inliers:" << vo->getNumberOfInliers() << endl;

        // get inliers
        vector<int32_t> inliers_ = vo->getInlierIndices();
        inliers.clear();
        for (int32_t i = 0; i < vo->getNumberOfMatches(); i++)
            inliers.push_back(false);
        for (std::vector<int32_t>::iterator it = inliers_.begin(); it != inliers_.end(); it++)
            inliers[*it] = true;

        // compute gain
        gain = vo->getGain(inliers_);


        if (H_inv.solve(H)) {
            H_total = H_total*H_inv;
            picked = false;
            emit newHomographyArrived();
            while (!picked) usleep(1000);
        }

    }
}

function [ matchCornerL, matchCornerR ] = feature_match( )
%FEATURE_MATCH Summary of this function goes here
%   Detailed explanation goes here

left = rgb2gray(imread('UI-L.jpg'));
right = rgb2gray(imread('UI-R.jpg'));

%left = imread('I1_000000.png');
%right = imread('I2_000000.png');

% Calculate corners for each image
corner_l = cornerFind(left);
corner_r = cornerFind(right);

% Calculate feature at each point (just the intensity values; simple)
W = 60;

leftpad = padarray(left,[W W],'circular');
rightpad = padarray(right,[W W],'circular');

featuresL = zeros( [size(corner_l,1), (2*W+1)^2+2]);
featuresR = zeros( [size(corner_r,1), (2*W+1)^2+2]);
for i = 1:size(featuresL,1)
    r = corner_l(i,2)+W;
    c = corner_l(i,1)+W;
    featuresL(i,:) = [double(reshape(leftpad(r-W:r+W,c-W:c+W),[1 (2*W+1)^2])),r, c];
end

for i = 1:size(featuresR,1)
    r = corner_r(i,2)+W;
    c = corner_r(i,1)+W;
    featuresR(i,:) = [double(reshape(rightpad(r-W:r+W,c-W:c+W),[1 (2*W+1)^2])),r,  c];
end

[IDX,D] = knnsearch(featuresR,featuresL);

% number of top features
numTop = 20;

[~,I] = sort(D,1);
I = I(1:numTop);

matchCornerL = zeros(numTop,2);
matchCornerR = zeros(numTop,2);

for i = 1:size(I,1)
   indexL = I(i);
   indexR = IDX(indexL);
   matchCornerL(i,:) = corner_l(indexL,1:2);
   matchCornerR(i,:) = corner_r(indexR,1:2);
end

close all;

combined = [left, right];
imshow(combined);
hold on;
for i = 1:size(matchCornerL,1)
   corL = matchCornerL(i,:);
   corR = matchCornerR(i,:);
   plot(corL(1), corL(2), 'r*');
   plot(corR(1)+size(left,2), corR(2), 'g*');
   plot([corL(1), corR(1)+size(left,2)],[corL(2),corR(2)]);
end


end


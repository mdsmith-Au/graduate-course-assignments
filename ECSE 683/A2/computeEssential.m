function [ E ] = computeEssential( F )
%COMPUTEESSENTIAL Summary of this function goes here
%   Detailed explanation goes here

% Intrinsic matrix
f = 30;
ox = 253;
oy = 189.5;
sx = 0.05;
sy = 0.05;
M = [-f/sx 0 ox; 0 -f/sy oy; 0 0 1];

E = M' * F * M;

end


function [ matchesL,matchesR ] = corr_matching(W,r)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
left = rgb2gray(imread('UI-L.jpg'));
right = rgb2gray(imread('UI-R.jpg'));

kernel = ones(2*W+1);

count = 1;
disparityIndex = zeros( [(2*r+1)^2, 2]);
ssdSums = zeros([size(right,1), size(right,2), count]);
for d1 = -r:r
    for d2 = -r:r
        disparityIndex(count,:) = [d1 d2];
        ssd = -(double(left) - double(circshift(right, [d1 d2]))).^2;
        ssdSums(:,:,count) = conv2(ssd,kernel,'same');
        count = count + 1;
    end
end

% Find maximum ssd sum across all disparities
[correlation,index] = max(ssdSums,[],3);

disparityy = reshape(disparityIndex(index,1),size(index));
disparityx = reshape(disparityIndex(index,2),size(index));

[maximums,ind] = max(correlation);
[sorted, sortInd] = sort(maximums,'descend');

top20Col = sortInd(1:20);
top20Rows = ind(top20Col);
top20dx = disparityx(sub2ind(size(disparityx),top20Rows,top20Col));
top20dy = disparityy(sub2ind(size(disparityy),top20Rows,top20Col));

matchesL = [top20Col; top20Rows]';
matchesR = [top20Col + top20dy; top20Rows + top20dx]';
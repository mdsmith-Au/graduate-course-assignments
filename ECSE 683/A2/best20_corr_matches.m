function [ best ] = best20_corr_matches( dx,dy,corr )
%BEST20_CORR_MATCHES Summary of this function goes here
%   Detailed explanation goes here

% Set 50 px. around edges to smallest possible value since there is no real
% correct correlation
% Unforuntately despite this we still get matches in the sky...
corr(1:50,:) = -realmax('double');
corr(end-50:end,:) = -realmax('double');
corr(:,1:50) = -realmax('double');
corr(:,end-50:end) = -realmax('double');

[maximums,ind] = max(corr);
[sorted, sortInd] = sort(maximums,'descend');

top20Col = sortInd(1:20);
top20Rows = ind(top20Col);
top20dx = dx(sub2ind(size(dx),top20Rows,top20Col));
top20dy = dy(sub2ind(size(dy),top20Rows,top20Col));
end


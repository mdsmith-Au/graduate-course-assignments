function [ ep1,ep2 ] = epipoles_location( F )
%EPIPOLES_LOCATION Summary of this function goes here
%   Detailed explanation goes here

[u, ~, v] = svd(F);
ep1 = v(:,3)/v(3,3);

ep2 = u(:,3)/u(3,3);

end


\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{mathtools}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\graphicspath{{figs/}}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{subcaption}
\usepackage[scientific-notation=true]{siunitx}
\usepackage[shortlabels]{enumitem}

\begin{document}
\title{Assignment 2 Report}
\date{\today}
\author{Michael Smith}
\maketitle

\section{Problem 1}
\paragraph{}
Using the correlation based method, we used SSD as both a matching and reliability metric as it provides a good measure of the similarity between a given patch in one image versus another.  With regards to being a reliability measure, the best points will have a greater value for the SSD as defined by Trucco \& Verri.  Fig. \ref{fig:corrmatch} shows the 20 best matches, all of which map one part of the sky to another.  This is unfortunate but not entirely unexpected, as the error with SSD (or any other method that measures basic differences in pixel values) will be minimal for the highly uniform areas of the sky.  One way to prevent this and achieve better correspondences would be to take the distribution of intensity in the windows under consideration into account; i.e. we discard matches that have highly uniform values in both images.  This goes beyond the scope of the algorithm as presented by Trucco \& Verri however and was not examined.
\paragraph{}
The feature based approach fared better than the correlation-based method.  The corner detector from the previous assignment was used.  For matching, a K-nearest neighbours algorithm was used to match vectors representing each feature.  Different vector types were considered, but the final implementation uses a very simple vector comprised of the intensity values in a window surrounding the detected corner and the x/y coordinates of the corner itself.  The distance (norm) between these vectors served as a reliability measure, with a distance of 0 being a perfect match.  The top 20 corners were picked as those with the least distance, and are shown in Fig. \ref{fig:featurematch}.  In that image we see that corners are correctly detected with no bad matches.  Not shown is the fact that if lower quality matches are considered the periodicity of the building causes issues; for example, the middle window on the top floor in the left image may match to the rightmost window on the top floor in the right image.  In such situations an algorithm such as RANSAC would need to be used.  Overall, the results are quite different from the those produced with correlation.


\section{Problem 2}
\paragraph{}
Looking at the data from Problem 1, we clearly see that the feature based matching produces more reliable results and as such they were initially used to estimate the Fundamental Matrix.  Unfortunately, later steps encountered issues as the points produced by feature-based matching tend to be clustered in an area in the upper part of the image and thereby in violation of some of the requirements of the eight point algorithm.  Manual point correlations were determined to work better; these are shown in Fig. \ref{fig:manualmatch}.  The matrix F is defined below:
\begin{equation}
F = \begin{bmatrix}
   \num{2.6450e-07} && \num{-1.6012e-05} && \num{3.9037e-03} \\
   \num{1.5320e-05} && \num{-1.0560e-07} &&  \num{2.3252e-03} \\
  \num{-4.0486e-03} && \num{-1.7431e-03} && \num{-1.8710e-01}
\end{bmatrix}
\end{equation}

The epipoles are below, specified in pixel coordinates.
\begin{equation}
	e_l=\begin{bmatrix}
 	-150.1170 \\
  	241.3252 \\
    1.0000
\end{bmatrix}
e_r=\begin{bmatrix}
 -110.6225 \\
  266.1833 \\
    1.0000
\end{bmatrix}
\end{equation}
With these epipoles, the epipolar lines were plotted.  They can be found in Figs. \ref{fig:left_ep} and \ref{fig:right_ep} for the left and right images respectively.

\paragraph{}
The Essential Matrix was calculated from F using the intrinsic parameters provided:
\begin{equation}
E = \begin{bmatrix}
   \num{9.5220e-02} && \num{-5.7642e+00} && \num{-5.6186e-01} \\
   \num{5.5151e+00} && \num{-3.8015e-02} &&  \num{-3.7087e+00} \\
  \num{6.4715e-01} && \num{3.4884e+00} && \num{-1.3348e-01}
\end{bmatrix}
\end{equation}

From this, R and T were estimated:
\begin{equation}
R = \begin{bmatrix}
   \num{-4.2139e-01} && \num{-5.8829e-02} && \num{9.1029e-01} \\
   \num{-6.5867e-02} && \num{-9.9147e-01} &&  \num{-8.6934e-02} \\
  \num{9.0923e-01} && \num{-9.1063e-02} && \num{4.1284e-01}
\end{bmatrix}
\end{equation}
\begin{equation}
\hat{T}=\begin{bmatrix}
   \num{5.6308e-01} \\
            \num{0} \\
   \num{8.2950e-01}
\end{bmatrix}
\end{equation}
\paragraph{}
Finally, using R and $\hat{T}$ we could rectify the images.  Unfortunately, an unknown error exists somewhere in either the rectification implementation itself or one the previous steps such as finding R and $\hat{T}$, leading to the incorrect rectification shown in Figs. \ref{fig:left_rect} and \ref{fig:right_rect} for the left and right images respectively.

\section{Problem 3}
Despite the problems with the 2nd question, we proceeded to reconstruct the 3D surface using the provided rectified images from myCourses.  We used the correlation-based method, as it provides the required dense set of correspondences and with a rectified image is very fast to run.  Fig. \ref{fig:disparity} shows the calculated depth as a 2D image for the purposes of this report.  There is a fair bit of noise present, which can be smoothed out by changing the minimum/maximum disparity value and the window size in which the correlations are found at the expense of smoothing over a lot of detail.  Considering the performance of even state-of-the art algorithms using only two images, the result is acceptable considering the simplicity of implementation compared to more modern algorithms.
\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{corr_match.png}
	\caption{The 20 "most reliable" pairs for correlation matching.}
	\label{fig:corrmatch}
\end{figure}

\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{feature_match.png}
	\caption{The 20 "most reliable" pairs for feature matching.}
	\label{fig:featurematch}
\end{figure}


\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{manual.png}
	\caption{20 pairs of features, manually matched.}
	\label{fig:manualmatch}
\end{figure}

\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{left_ep.png}
	\caption{The epipolar lines for the left image.}
	\label{fig:left_ep}
\end{figure}

\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{right_ep.png}
	\caption{The epipolar lines for the right image.}
	\label{fig:right_ep}
\end{figure}
\clearpage


\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{rect.png}
	\caption{The (incorrectly) rectified left image.}
	\label{fig:left_rect}
\end{figure}


\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{right_rect.png}
	\caption{The (incorrectly) rectified right image.}
	\label{fig:right_rect}
\end{figure}


\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{disparity.png}
	\caption{The disparity calculated using the provided rectified images. A window size and maximum disparity of 20 were used.}
	\label{fig:disparity}
\end{figure}
\end{document}

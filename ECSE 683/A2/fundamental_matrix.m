function [ F ] = fundamental_matrix( )
%FUNDAMENTAL_MATRIX Summary of this function goes here
%   Detailed explanation goes here

% Use eight point algorithm
% [pointL, pointR] = feature_match();
% 
% % Pick best n pts
% n = 20;
% pointL = pointL(1:n,:);
% pointR = pointR(1:n,:);

load('handpts.mat');

% Homogenize 2D points (add f for Z coord)
pointLHomog = [pointL, ones(size(pointL,1),1)];
pointRHomog = [pointR, ones(size(pointR,1),1)];

% Normalize pts
[p1_norm,t1] = normalize_points(pointLHomog);
[p2_norm,t2] = normalize_points(pointRHomog);


x1 = p1_norm(:,1);
y1 = p1_norm(:,2);
x2 = p2_norm(:,1);
y2 = p2_norm(:,2);

% Number of points
numPts = size(p1_norm,1);

A = [x2.*x1 x2.*y1 x2 y2.*x1 y2.*y1 y2 x1 y1 ones(numPts,1)];

% Singular Value Decomposition of A.
[u, d, v] = svd(A);

% Obtain F from 9th column of V (the smallest singular value of A).
F = v(:,9);
% Could use reshape but the last time I did I spent hours on Levine's
% assignment tracking down a bug because of it...
F = [F(1) F(2) F(3); F(4) F(5) F(6); F(7) F(8) F(9)];

% Singularity constraint enforcement. 
[u, d, v] = svd(F);
d(3,3) = 0;


%Corrected estimate
F = u * d * v';

% Denormalization
F = t2' * F * t1;
end


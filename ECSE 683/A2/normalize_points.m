function [ normalized, H ] = normalize_points( points )
%NORMALIZE_POINTS Summary of this function goes here
%   Detailed explanation goes here

% find centroid
cent = mean(points(:,1:2));

shifted(:,1) = points(:,1) - cent(1);
shifted(:,2) = points(:,2) - cent(2);

dist = sqrt(shifted(:,1).^2 + shifted(:,2).^2);
meandist = mean(dist(:));

scale = sqrt(2)/meandist;

H = [scale   0   -scale*cent(1);  0     scale -scale*cent(2);  0       0      1      ];

normalized = (H*points')';

end


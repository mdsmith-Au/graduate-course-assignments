function [  ] = draw_epipoles( img,F,pts,imageLoc )
%EPIPOLES_LOCATION Summary of this function goes here
%   Detailed explanation goes here
% [u, ~, v] = svd(F);
% 
% epipole_l = v(:,3)/v(3,3);
% epipole_r = u(:,3)/u(3,3);

pts = [pts, ones(size(pts,1),1)];

pts = pts(1:5,:);
%pts = pts(1:15,:);
imshow(img);
hold on;
for i=1:size(pts,1)

    % Define lines
    if strcmp(imageLoc,'right')
        line = F * pts(i,:)';
    else
        line = F' * pts(i,:)';
    end

    %Define line: y = (-ax - c) / b

    x = [0 size(img,2)];
    y = (-line(1)*x - line(3)) / line(2);

    
	plot(x, y);
end


end


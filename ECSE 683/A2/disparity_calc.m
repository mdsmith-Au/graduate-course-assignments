function [ disparity ] = disparity_calc( W,r )
%DISPARITY_CALC Summary of this function goes here
%   Detailed explanation goes here
left = rgb2gray(imread('RI-L.jpg'));
right = rgb2gray(imread('RI-R.jpg'));

% Crop left, right images
left = imcrop(left,[40 45 450 335]);
right = imcrop(right,[40 45 450 335]);

kernel = ones(2*W+1);

imgSize = imref2d(size(left));

count = 1;
disparityIndex = zeros( 2*r+1,1);
ssdSums = zeros([size(right,1), size(right,2), count]);
for d1 = -r:r
        disparityIndex(count) = d1;
        shift = [ 1 0 0; 0 1 0; d1 0 1];
        tform = affine2d(shift);
        ssd = -(double(left) - double(imwarp(right, tform,'OutputView',imgSize))).^2;
        ssdSums(:,:,count) = conv2(ssd,kernel,'same');
        count = count + 1;
end

% Find maximum ssd sum across all disparities
[correlation,index] = max(ssdSums,[],3);

disparity = reshape(disparityIndex(index),size(index));
% dispcrop = imcrop(disparity,[75 15 375 330]);
% imagesc(dispcrop);
imagesc(disparity);


end


function [ T_norm,R ] = lastpart( E, ptsL, ptsR )
%LASTPART Summary of this function goes here
%   Detailed explanation goes here

% % Test!!
%  W = [0 -1 0; 1 0 0 ; 0 0 1];
%  Z = [0 1 0; -1 0 0; 0 0 0];
% 
%  [u,d,v] = svd(E);
% 
%  R = u*W'*v';
%  T_cx = u * Z * u';
% %T_cx = u * W * d * u';
% 
%  Tx = T_cx(3,2);
%  Ty = T_cx(1,3);
%  Tz = T_cx(2,1);
% 
%  T_norm = [Tx;Ty;Tz];

f = 30;

N = sqrt(trace(E'*E)/2);

E_norm = E ./ N;

eq_726 = E_norm' * E_norm;

% Could be done via matrix operations ...
Tx_norm = real(sqrt(-(eq_726(1,1) - 1)));
Ty_norm = real(sqrt(-(eq_726(2,2) - 1)));
Tz_norm = real(sqrt(-(eq_726(3,3) - 1)));
% 
T_norm = [Tx_norm; Ty_norm; Tz_norm];

%% Step 2
w_1 = cross( E_norm(1,:)', T_norm);
w_2 = cross( E_norm(2,:)', T_norm);
w_3 = cross( E_norm(3,:)', T_norm);

% Eq. 7.28
R_1 = w_1 + cross(w_2,w_3);
R_2 = w_2 + cross(w_3,w_1);
R_3 = w_3 + cross(w_1,w_2);

R = [R_1'; R_2'; R_3'];


% Reconstruct points and evaluate which of the 4 possibilities it is for
% R,T
repeat = 1;
numPositive = 0;
numExecutions = 0;
while repeat == 1
    for i = 1 : size(ptsR,1)
       p_l = ptsL(i);
       p_l(3) = f;
       top = (f * R_1 - ptsR(i,1)*R_3)'*T_norm;
       bottom = (f * R_1 - ptsR(i,1)*R_3)'*p_l';
       Z_l = f * top/bottom;
       P_l = p_l' * f/Z_l;
       Z_r = R_3'*(P_l - T_norm);

       %Case a
      if numExecutions > 3
           repeat = 0;
           disp('over execution limit');
           break;
      elseif Z_r < 0 && Z_l < 0
           T_norm = -T_norm;
           numPositive = 0;
           numExecutions = numExecutions + 1;
           disp('case a');
           break;
       % Case b
       elseif (Z_l < 0 && Z_r > 0) || (Z_l > 0 && Z_r < 0) 
            E_norm = -E_norm;
            
            w_1 = cross( E_norm(1,:)', T_norm);
            w_2 = cross( E_norm(2,:)', T_norm);
            w_3 = cross( E_norm(3,:)', T_norm);

            % Eq. 7.28
            R_1 = w_1 + cross(w_2,w_3);
            R_2 = w_2 + cross(w_3,w_1);
            R_3 = w_3 + cross(w_1,w_2);

            R = [R_1'; R_2'; R_3'];
            numPositive = 0;
            numExecutions = numExecutions + 1;
            disp('case b');
            break;
       % Case c
       elseif Z_l > 0 && Z_r > 0
            numPositive = numPositive + 1;
            if (numPositive == size(ptsR,1))
                repeat = 0;
                disp('all +ve');
                break;
            end
       end
    end
end

% Rectification
e_1 = T_norm;
e_2 = ( 1/sqrt(T_norm(1)^2 + T_norm(2)^2) ) * [-T_norm(2), T_norm(1),0]';
e_3 = cross(e_1,e_2);

R_rect = [e_1'; e_2'; e_3'];

R_l = R_rect;
R_r = R*R_rect;

left = rgb2gray(imread('UI-L.jpg'));
right = rgb2gray(imread('UI-R.jpg'));

%left = imread('I1_000000.png');
%right = imread('I2_000000.png');

rectL = zeros(size(left),'uint8');
rectR = zeros(size(right),'uint8');

center = [size(left,2)/2, size(left,1)/2];

f = 30;

for i = 1 : size(rectL,1)
    for j = 1:size(rectL,2)
       
        p_l = [j-center(1);i-center(2);f];
%       p_l = [j;i;f];

       pt = R_l * p_l;
       p_l_prime = pt(3)/f * pt;
        p_l_prime(1) = p_l_prime(1) + center(1);
        p_l_prime(2) = p_l_prime(2) + center(2);
       p_l_prime = round(p_l_prime);
       if (p_l_prime(1) > 0 && p_l_prime(2) > 0 && p_l_prime(1) < size(left,2) && p_l_prime(2) < size(left,1))
           rectL(i,j) = left(p_l_prime(2),p_l_prime(1));
       end
    end
end
close all;
imshow(rectL);
title('Left, rectified');

for i = 1 : size(rectR,1)
    for j = 1:size(rectR,2)
       
        p_r = [j-center(1);i-center(2);f];
%       p_l = [j;i;f];

       pt = R_r * p_r;
       p_r_prime = pt(3)/f * pt;
        p_r_prime(1) = p_r_prime(1) + center(1);
        p_r_prime(2) = p_r_prime(2) + center(2);
       p_r_prime = round(p_r_prime);
       if (p_r_prime(1) > 0 && p_r_prime(2) > 0 && p_r_prime(1) < size(right,2) && p_r_prime(2) < size(right,1))
           rectR(i,j) = right(p_r_prime(2),p_r_prime(1));
       end
    end
end

figure;
imshow(rectR);
title('Right, rectified');

% for i = 1 : size(left,1)
%     for j = 1:size(left,2)
%        
%        p_l = [j-center(1);i-center(2);f];
%        p_l = [j;i;f];
% 
%        pt = R_l * p_l;
%        p_l_prime = f/pt(3) * pt;
%        p_l_prime(1) = p_l_prime(1) + center(1);
%        p_l_prime(2) = p_l_prime(2) + center(2);
%        p_l_prime = round(p_l_prime);
%        if (p_l_prime(1) > 0 && p_l_prime(2) > 0 && p_l_prime(1) < size(left,2) && p_l_prime(2) < size(left,1))
%            rectL(p_l_prime(2),p_l_prime(1)) = left(i,j);
%        end
%     end
% end
end

